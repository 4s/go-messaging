package messaging

import (
	"context"
	"errors"
	"os"
	"strconv"
	"sync"
	"time"

	log "bitbucket.org/4s/go-logging"
)

var enableHealth bool
var healthFilePath string
var lastHealthTime time.Time
var healthIntervalDuration time.Duration

// ConsumeAndProcess is a struct for consuming, processing and responding to messages over kafka
type ConsumeAndProcess struct {
	interceptors []EventConsumerInterceptor
	consumer     EventConsumer
	producer     EventProducer
	outputTopic  Topic
}

// NewConsumeAndProcess functions as a constructor that returns a new instance of ConsumeAndProcess
func NewConsumeAndProcess(
	ctx context.Context,
	consumer EventConsumer,
	producer EventProducer,
) (ConsumeAndProcess, error) {
	var err error
	enableHealth, err = strconv.ParseBool(GetEnv(ctx, "ENABLE_HEALTH", "false"))
	if err != nil {
		enableHealth = false
	}
	if enableHealth {
		healthFilePath = GetEnv(ctx, "HEALTH_FILE_PATH", "")
		if healthFilePath == "" {
			log.WithContext(ctx).Warnf("Health is enabled but HEALTH_FILE_PATH is undefined")
			enableHealth = false
		}
		healthIntervalMSString := GetEnv(ctx, "HEALTH_INTERVAL_MS", "")
		if healthIntervalMSString == "" {
			log.WithContext(ctx).Warnf("Health is enabled but HEALTH_INTERVAL_MS is undefined")
			enableHealth = false
		}
		healthIntervalMS, err := strconv.ParseInt(healthIntervalMSString, 10, 32)
		if err != nil {
			log.WithContext(ctx).Warnf("Health is enabled but an error occurred while parsing HEALTH_INTERVAL_MS: %v", err)
			enableHealth = false
		}
		healthIntervalDuration = time.Duration(healthIntervalMS) * time.Millisecond
	}
	outputTopic := NewTopic()

	consumeAndProcess := ConsumeAndProcess{
		nil,
		consumer,
		producer,
		outputTopic,
	}

	return consumeAndProcess, nil
}

// Run is a method that runs the consumeAndProcess method in a loop until it is stopped
// This method should be run in a dedicated goroutine
func (consumeAndProcess *ConsumeAndProcess) Run(
	ctx context.Context,
	sigs chan os.Signal,
	wg *sync.WaitGroup,
) {
	for {
		select {
		case <-sigs:
			log.WithContext(ctx).Debug("Stop signal received")
			consumeAndProcess.stop()
			wg.Done()
			return
		default:
			err := consumeAndProcess.consumeAndProcess(ctx)
			healthy(ctx)
			if err != nil {
				log.WithContext(ctx).Errorf("an error occurred while running consumeAndProcess: %v", err.Error())
				consumeAndProcess.stop()
				wg.Done()
			}
		}
	}
}

// consumeAndProcess consumes, processes and responds to messages over Kafka.
// Using a KafkaConsumer consumeAndProcess polls Kafka on the specified inputtopic.
// It the relays any received messages to a KafkaEventProcessor which returns a new message
// The new message is then passed on to a KafkaProducer, that produces it on the outputtopic
func (consumeAndProcess *ConsumeAndProcess) consumeAndProcess(ctx context.Context) error {
	if consumeAndProcess.consumer == nil {
		return errors.New("no EventConsumer defined")
	}

	message, topic, err := consumeAndProcess.consumer.Poll(ctx)
	if err != nil {
		log.WithContext(ctx).Error("an error occurred while polling messages", err)
		return consumeAndProcess.handleError(ctx, topic, message, err)
	}
	if (message == Message{}) {
		return nil
	}

	message.SetCorrelationID(VerifyOrCreateID(message.GetCorrelationID()))

	CorrelationIDKey := GetEnv(ctx, "CORRELATION_ID", "correlationId")
	TransactionIDKey := GetEnv(ctx, "TRANSACTION_ID", "transactionId")

	// FIXME: Should probably be moved to service
	ctx = log.NewContext(ctx, map[string]interface{}{
		CorrelationIDKey: message.GetCorrelationID(),
		TransactionIDKey: message.GetTransactionID(),
	})

	log.WithContext(ctx).Debug("message received from Kafka with correlationID: " + message.GetCorrelationID())

	outgoingMessage := NewMessage()
	err = outgoingMessage.CloneFields(message)
	if err != nil {
		log.WithContext(ctx).Error("an error occurred while cloning fields from incoming message", err)
		return consumeAndProcess.handleError(ctx, topic, message, err)
	}
	outgoingMessage.SetBody("")

	err = consumeAndProcess.consumer.ProcessMessage(ctx, topic, message, &consumeAndProcess.outputTopic, &outgoingMessage)
	if err != nil {
		log.WithContext(ctx).Warn("processing stopped due to an error")
		return consumeAndProcess.handleError(ctx, topic, message, err)
	}
	if consumeAndProcess.producer == nil {
		log.WithContext(ctx).Debug("no KafkaEventProducer defined. Not outputting to Kafka")
		return nil
	}
	if consumeAndProcess.outputTopic.Equals(Topic{}) {
		log.WithContext(ctx).Debug("no output topic. Not outputting to Kafka")
		return nil
	}
	if outgoingMessage == (Message{}) {
		log.WithContext(ctx).Debug("no outgoing message. Not outputting to Kafka")
		return nil
	}
	if err == nil {
		if consumeAndProcess.interceptors != nil {
			for _, next := range consumeAndProcess.interceptors {
				next.ProcessingCompletedNormally(topic, message)
			}
		}

		consumeAndProcess.consumer.Commit()
		consumeAndProcess.producer.Produce(consumeAndProcess.outputTopic, outgoingMessage)
	}
	return nil
}

func healthy(ctx context.Context) {
	if enableHealth {
		currenttime := time.Now()
		elapsed := currenttime.Sub(lastHealthTime)
		if elapsed.Seconds() > healthIntervalDuration.Seconds() {
			// detect if file exists
			var _, err = os.Stat(healthFilePath)
			var file *os.File
			// create file if not exists
			if os.IsNotExist(err) {
				file, err = os.Create(healthFilePath)
				if err != nil {
					log.WithContext(ctx).Warnf("cannot create health file. disabling off Health")
					enableHealth = false
				}
				defer file.Close()
			} else {
				file, err = os.Open(healthFilePath)
				if err != nil {
					log.WithContext(ctx).Warnf("cannot open health file. disabling off Health")
					enableHealth = false
				}
			}
			if file != nil {
				err := os.Chtimes(file.Name(), currenttime, currenttime)
				if err != nil {
					log.WithContext(ctx).Debugf("could not change modified time of healthfile: %v", err)
				}
				lastHealthTime = currenttime
			}
		}
	}
}

func (consumeAndProcess *ConsumeAndProcess) handleError(ctx context.Context, topic Topic, message Message, err error) error {
	if consumeAndProcess.interceptors != nil {
		for _, next := range consumeAndProcess.interceptors {
			continueProcessing := next.HandleError(topic, message, err)
			if !continueProcessing {
				log.WithContext(ctx).Error("an error occurred while polling messages", err)
			}
		}
	}
	return err
}

// GetInterceptors returns a list of all registered server interceptors
func (consumeAndProcess *ConsumeAndProcess) GetInterceptors() []EventConsumerInterceptor {
	return consumeAndProcess.interceptors
}

// SetInterceptors sets (or clears) the list of interceptors
func (consumeAndProcess *ConsumeAndProcess) SetInterceptors(interceptors ...EventConsumerInterceptor) {
	consumeAndProcess.interceptors = interceptors
	// set consumer hooks
	consumeAndProcess.consumer.SetPreParseHook(func(topic Topic, message Message) bool {
		var continueProcessing bool
		for _, next := range consumeAndProcess.interceptors {
			continueProcessing = next.IncomingMessagePreParsing(topic, message)
			if !continueProcessing {
				break
			}
		}
		return continueProcessing
	})

	consumeAndProcess.consumer.SetPreProcessHook(func(topic Topic, message Message) bool {
		var continueProcessing bool
		for _, next := range consumeAndProcess.interceptors {
			continueProcessing = next.IncomingMessagePreProcessMessage(topic, message)
			if !continueProcessing {
				break
			}
		}
		return continueProcessing
	})

	consumeAndProcess.consumer.SetPostProcessHook(func(topic Topic, message Message) bool {
		var continueProcessing bool
		for _, next := range consumeAndProcess.interceptors {
			continueProcessing = next.IncomingMessagePostProcessMessage(topic, message)
			if !continueProcessing {
				break
			}
		}
		return continueProcessing
	})
}

// stop closes the KafkaProducer and KafkaConsumer
func (consumeAndProcess *ConsumeAndProcess) stop() {
	if consumeAndProcess.consumer != nil {
		consumeAndProcess.consumer.Close()
	}
	if consumeAndProcess.producer != nil {
		consumeAndProcess.producer.Close()
	}
}
