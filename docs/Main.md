# Go Messaging library

[TOC]

A library that supports sending and receiving messages over the Apache Kafka messaging system. 

The library implements structs for message consumption, for message production and for serializing and deserializing
messages and topics.

This library is a reimplementation of an already existing Java library (https://bitbucket.org/4s/messaging)
Therefore throughout this documentation, the original library will be referenced.

## Topics
Topics can be serialized using the [Topic](https://bitbucket.org/4s/go-messaging/src/master/topic.go)
struct. Topics can be deserialized using the method `TopicFromString` from the [topic_parser.go](https://bitbucket.org/4s/go-messaging/src/master/topic_parser.go)
file.

More on topics here: [Java Messaging library](https://bitbucket.org/4s/messaging/src/master/docs/Main.md)

## Messages

Info on Messages here: [Java Messaging library](https://bitbucket.org/4s/messaging/src/master/docs/Main.md)

## Kafka implementation

### Producing messages
To produce messages to Kafka instantiate a [KafkaEventProducer](https://bitbucket.org/4s/go-messaging/src/master/kafka_event_producer.go)
 struct and use KafkaEventProducer.Produce to send messages to Kafka on a particular topic:

```golang
#!golang
...
message := messaging.NewMessage()
  message.SetBody("Testbody").
    SetSender("My message sender").
    SetCorrelationID().
    SetTransactionID().
    SetBodyCategory(messaging.FHIR).
    SetBodyType("Observation").
    SetContentVersion(messaging.GetEnv("FHIR_VERSION", ""))
  
  kafkaEventProducer, err := messaging.NewKafkaEventProducer(ctx, "My message sender")
  if err != nil {
    return fmt.Errorf("could not create kafkaEventProducer: %v", err)
  }
  err = kafkaEventProducer.Produce(topic, message)
  if err != nil {
    return fmt.Errorf("could not produce with kafkaEventProducer: %v", err)
  }
```

### Consuming messages
The struct [KafkaConsumeAndProcess](https://bitbucket.org/4s/go-messaging/src/master/kafka_consume_and_process.go)
handles consuming, processing and responding to messages.

To use this class you first need to define a struct that implements the interface [EventProcessor](https://bitbucket.org/4s/go-messaging/src/master/event_processor.go).
 The implementation of processMessage should contain the main processing of the body of the received message:

```golang
type MyEventProcessor struct{}

func (mep MyEventProcessor) ProcessMessage(
	ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message,
) error {
  ... // process the body of the received message
  return nil
}
```
Second, you need an instance of the [KafkaEventProducer](https://bitbucket.org/4s/go-messaging/src/master/kafka_event_producer.go)
struct. This instance is for use by *KafkaConsumeAndProcess* for producing result messages.

```golang
kafkaEventProducer, err := messaging.NewKafkaEventProducer(ctx, "My message sender")
  if err != nil {
    return fmt.Errorf("could not create kafkaEventProducer: %v", err)
  }
```

Finally, you need an instance of the [KafkaEventConsumer](https://bitbucket.org/4s/go-messaging/src/master/kafka_event_consumer.go) struct. 
This instance is for use by *KafkaConsumeAndProcess* for consuming messages:

```golang
kafkaEventConsumer, err := messaging.NewKafkaEventConsumer(ctx)
if err != nil {
  return fmt.Errorf("could not create kafkaEventConsumer: %v", err)
}

// list of topics
topic := messaging.NewTopic()
topic.SetOperation(messaging.InputReceived).
  SetDataCategory(messaging.FHIR).
  SetDataType("QuestionnaireResponse")
topics := messaging.Topics{}
topics = append(topics, topic)
kafkaEventConsumer.SubscribeTopics(topics)

// or topic pattern
regex := regexp.MustCompile("InputReceived_FHIR_QuestionnaireResponse(.*)")
kafkaEventConsumer.SubscribeTopicPattern(regex)
```

Example instantiation of *KafkaConsumeAndProcess*:

```golang
eventProcessor, _ := NewMyEventProcessor(ctx)
kafkaEventProducer, err := messaging.NewKafkaEventProducer(ctx, "My message sender")
if err != nil {
  return fmt.Errorf("could not create kafkaEventProducer: %v", err)
}

kafkaEventConsumer, err := messaging.NewKafkaEventConsumer(ctx)
if err != nil {
  return fmt.Errorf("could not create kafkaEventConsumer: %v", err)
}
regex := regexp.MustCompile("InputReceived_FHIR_QuestionnaireResponse(.*)")
kafkaEventConsumer.SubscribeTopicPattern(regex)

kafkaConsumeAndProcess, err := messaging.NewKafkaConsumeAndProcess(
  ctx,
  &kafkaEventConsumer,
  &kafkaEventProducer,
  eventProcessor,
)
if err != nil {
  return fmt.Errorf("could not create kafkaConsumeAndProcess: %v", err)
}
```

## Mock implementation
This library also contains a mock implementation of messaging, to be used in tests. The mock implementation
functions in mostly the same way as the Kafka implementation

### Producing messages
To produce messages to Kafka instantiate a [MockEventProducer](https://bitbucket.org/4s/go-messaging/src/master/mock/mock_event_producer.go)
 struct and use MockEventProducer.Produce to send messages to Kafka on a particular topic:

```golang
#!golang
...
message := messaging.NewMessage()
  message.SetBody("Testbody").
    SetSender("My message sender").
    SetCorrelationID().
    SetTransactionID().
    SetBodyCategory(messaging.FHIR).
    SetBodyType("Observation").
    SetContentVersion(messaging.GetEnv("FHIR_VERSION", ""))
  
  mockEventProducer, err := mock.NewMockEventProducer(ctx, "My message sender")
  if err != nil {
    return fmt.Errorf("could not create mockEventProducer: %v", err)
  }
  err = mockEventProducer.Produce(topic, message)
  if err != nil {
    return fmt.Errorf("could not produce with mockEventProducer: %v", err)
  }
```

### Consuming messages
The struct [KafkaConsumeAndProcess](https://bitbucket.org/4s/go-messaging/src/master/kafka_consume_and_process.go)
also handles consuming, processing and responding to messages in the Mock implementation.

To use this struct with the mock implementation you also need to define a struct that implements the interface [EventProcessor](https://bitbucket.org/4s/go-messaging/src/master/event_processor.go).
 The implementation of processMessage should contain the main processing of the body of the received message:

```golang
type MyEventProcessor struct{}

func (mep MyEventProcessor) ProcessMessage(
	ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message,
) error {
  ... // process the body of the received message
  return nil
}
```
Second, you need an instance of the [MockEventProducer](https://bitbucket.org/4s/go-messaging/src/master/mock_event_producer.go)
struct:

```golang
mockEventProducer, err := mock.NewMockEventProducer(ctx, "My message sender")
if err != nil {
  return fmt.Errorf("could not create mockEventProducer: %v", err)
}
```

Finally, you need an instance of the [MockEventConsumer](https://bitbucket.org/4s/go-messaging/src/master/mock/mock_event_consumer.go) struct:

```golang
mockEventConsumer, err := mock.NewMockEventConsumer(ctx)
if err != nil {
  return fmt.Errorf("could not create mockEventConsumer: %v", err)
}

// list of topics
topic := messaging.NewTopic()
topic.SetOperation(messaging.InputReceived).
  SetDataCategory(messaging.FHIR).
  SetDataType("QuestionnaireResponse")
topics := messaging.Topics{}
topics = append(topics, topic)
mockEventConsumer.SubscribeTopics(topics)

// or topic pattern
regex := regexp.MustCompile("InputReceived_FHIR_QuestionnaireResponse(.*)")
mockEventConsumer.SubscribeTopicPattern(regex)
```

Example instantiation of *KafkaConsumeAndProcess* with Mock implementation:

```golang
eventProcessor, _ := NewMyEventProcessor(ctx)
mockEventProducer, err := mock.NewMockEventProducer(ctx, "My message sender")
if err != nil {
  return fmt.Errorf("could not create mockEventProducer: %v", err)
}

mockEventConsumer, err := mock.NewMockEventConsumer(ctx)
if err != nil {
  return fmt.Errorf("could not create kafkaEventConsumer: %v", err)
}
regex := regexp.MustCompile("InputReceived_FHIR_QuestionnaireResponse(.*)")
mockEventConsumer.SubscribeTopicPattern(regex)

kafkaConsumeAndProcess, err := messaging.NewKafkaConsumeAndProcess(
  ctx,
  &mockEventConsumer,
  &mockEventProducer,
  eventProcessor,
)
if err != nil {
  return fmt.Errorf("could not create kafkaConsumeAndProcess: %v", err)
}
```