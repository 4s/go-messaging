package messaging

import (
	"bytes"
	"encoding/json"
	"errors"
	"regexp"
	"strings"
)

// VERSION is the current version of the message format
// Change this whenever you make changes to the Message format! Uses semantic versioning
var VERSION = NewVersion("3.0.0")

// Message is struct for producing messages on the form:
// {
//   "header":
//   {
//     "sender": "obs-input-service",
//     "bodyCategory": "FHIR",
//     "bodyType": "Observation",
//     "contentVersion": "3.3.0",
//     "location":"http://fhirtest.uhn.ca/baseDstu3/Observation/15354/_history/1"
//     "correlationId": "xez5ZXQcDG6"
//     "transactionId": "e6651fe2-fb8e-4a54-8b8e-7343dbdb997c"
//     "security": ...
//   },
//   "body":
//   {
//     "resourceType": "Observation",
//     ...
//   }
// }
type Message struct {
	Header Header `json:"header,omitempty"`
	Body   string `json:"body,omitempty"`
}

// Header is the header of the message
type Header struct {
	HeaderVersion  Version      `json:"version,omitempty"`
	Sender         string       `json:"sender,omitempty"`
	BodyCategory   BodyCategory `json:"bodyCategory,omitempty"`
	BodyType       string       `json:"bodyType,omitempty"`
	ContentVersion string       `json:"contentVersion,omitempty"`
	Prefer         Prefer       `json:"prefer,omitempty"`
	Etag           string       `json:"etag,omitempty"`
	IfMatch        string       `json:"ifMatch,omitempty"`
	IfNoneExist    string       `json:"ifNoneExist,omitempty"`
	Location       string       `json:"location,omitempty"`
	CorrelationID  string       `json:"correlationId,omitempty"`
	TransactionID  string       `json:"transactionId,omitempty"`
	Security       string       `json:"security,omitempty"`
	Session        string       `json:"session,omitempty"`
}

// BodyCategory is an enum for the set of valid message bodyCategories.
type BodyCategory int

// The valid bodyCategory values
const (
	FHIR BodyCategory = iota + 1
	CDA
	System
)

func (b BodyCategory) String() string {
	names := [...]string{
		"",
		"FHIR",
		"CDA",
		"System"}

	if b > System || b < FHIR {
		return "Unknown"
	}

	return names[b]
}

// MarshalJSON marshals BodyCategory as a quoted json string
func (b BodyCategory) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(b.String())
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

// UnmarshalJSON unmashals a quoted json string to BodyCategory
func (b *BodyCategory) UnmarshalJSON(byteArray []byte) error {
	var j string
	err := json.Unmarshal(byteArray, &j)
	if err != nil {
		return err
	}
	// Note that if the string cannot be found then it will be set to the zero value, 'Created' in this case.
	*b, err = BodyCategoryFromString(j)
	if err != nil {
		return err
	}
	return nil
}

// Prefer is an enum for the set of valid message prefers.
type Prefer int

// The valid prefer values
const (
	REPRESENTATION Prefer = iota + 1
	OPERATIONOUTCOME
	MINIMAL
)

func (p Prefer) String() string {
	names := [...]string{
		"",
		"REPRESENTATION",
		"OPERATIONOUTCOME",
		"MINIMAL"}

	if p > MINIMAL || p < REPRESENTATION {
		return "Unknown"
	}

	return names[p]
}

// MarshalJSON marshals Prefer as a quoted json string
func (p Prefer) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(p.String())
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

// UnmarshalJSON unmashals a quoted json string to Prefer
func (p *Prefer) UnmarshalJSON(byteArray []byte) error {
	var j string
	err := json.Unmarshal(byteArray, &j)
	if err != nil {
		return err
	}
	// Note that if the string cannot be found then it will be set to the zero value, 'Created' in this case.
	*p, err = PreferFromString(j)
	if err != nil {
		return err
	}
	return nil
}

// NewMessage functions as a constructor for Message that returns a new instance of Message
func NewMessage() Message {
	m := Message{}
	m.SetHeaderVersion(VERSION)
	return m
}

// CloneFields copies the fields from the provided message, to this message.
func (m *Message) CloneFields(message Message) error {
	if !m.GetHeaderVersion().IsCompatible(message.GetHeaderVersion()) {
		return errors.New(
			"Message versions are incompatible. This version = " +
				m.GetHeaderVersion().String() + " other version = " +
				message.GetHeaderVersion().String())
	}
	m.SetHeaderVersion(message.GetHeaderVersion())
	m.SetSender(message.GetSender())
	m.SetBodyCategory(message.GetBodyCategory())
	m.SetBodyType(message.GetBodyType())
	m.SetContentVersion(message.GetContentVersion())
	m.SetPrefer(message.GetPrefer())
	m.SetEtag(message.GetEtag())
	m.SetIfMatch(message.GetIfMatch())
	m.SetLocation(message.GetLocation())
	m.SetCorrelationID(message.GetCorrelationID())
	m.SetTransactionID(message.GetTransactionID())
	m.SetSecurity(message.GetSecurity())
	m.SetSession(message.GetSession())
	m.SetBody(message.GetBody())

	return nil
}

// SetHeaderVersion sets the headerVersion of the message
func (m *Message) SetHeaderVersion(version Version) *Message {
	m.Header.HeaderVersion = version
	return m
}

// GetHeaderVersion returns the headerVersion of the message
func (m *Message) GetHeaderVersion() Version {
	return m.Header.HeaderVersion
}

// SetSender sets the sender of the message
func (m *Message) SetSender(sender string) *Message {
	m.Header.Sender = sender
	return m
}

// GetSender returns the sender of the message
func (m *Message) GetSender() string {
	return m.Header.Sender
}

// SetBodyCategory sets the bodyCategory of the message
func (m *Message) SetBodyCategory(bodyCategory BodyCategory) *Message {
	m.Header.BodyCategory = bodyCategory
	return m
}

// GetBodyCategory returns the bodyCategory of the message
func (m *Message) GetBodyCategory() BodyCategory {
	return m.Header.BodyCategory
}

// BodyCategoryFromString parses a string into a BodyCategory
func BodyCategoryFromString(catString string) (BodyCategory, error) {
	var bodyCategory BodyCategory
	switch true {
	case catString == FHIR.String():
		bodyCategory = FHIR
		return bodyCategory, nil
	case catString == CDA.String():
		bodyCategory = CDA
		return bodyCategory, nil
	case catString == System.String():
		bodyCategory = System
		return bodyCategory, nil
	default:
		return bodyCategory, errors.New("Unknown BodyCategory: " + catString)
	}
}

// SetBodyType sets the bodytype of the message
func (m *Message) SetBodyType(bodyType string) *Message {
	m.Header.BodyType = bodyType
	return m
}

// GetBodyType returns the bodytype of the message
func (m *Message) GetBodyType() string {
	return m.Header.BodyType
}

// SetContentVersion sets the content version of the message
func (m *Message) SetContentVersion(contentVersion string) *Message {
	m.Header.ContentVersion = contentVersion
	return m
}

// GetContentVersion returns the content version of the message
func (m *Message) GetContentVersion() string {
	return m.Header.ContentVersion
}

// SetPrefer sets the prefer header of the message
func (m *Message) SetPrefer(prefer Prefer) *Message {
	m.Header.Prefer = prefer
	return m
}

// GetPrefer returns the prefer header of the message
func (m *Message) GetPrefer() Prefer {
	return m.Header.Prefer
}

// PreferFromString parses a string into a prefer
func PreferFromString(catString string) (Prefer, error) {
	var prefer Prefer
	if catString == "" {
		return prefer, errors.New("prefer empty")
	}
	if !strings.Contains(catString, "=") {
		return prefer, errors.New("malformed prefer: missing \"=\"")
	}
	s := strings.Split(catString, "=")
	if s[0] != "return" {
		return prefer, errors.New("malformed prefer: missing \"return\"")
	}

	switch true {
	case strings.ToUpper(s[1]) == strings.ToUpper(REPRESENTATION.String()):
		prefer = REPRESENTATION
		return prefer, nil
	case strings.ToUpper(s[1]) == strings.ToUpper(OPERATIONOUTCOME.String()):
		prefer = OPERATIONOUTCOME
		return prefer, nil
	case strings.ToUpper(s[1]) == strings.ToUpper(MINIMAL.String()):
		prefer = MINIMAL
		return prefer, nil
	default:
		return prefer, errors.New("Unknown prefer")
	}
}

// SetEtag sets the etag header of the message
func (m *Message) SetEtag(etag string) *Message {
	m.Header.Etag = etag
	return m
}

// GetEtag returns the etag header of the message
func (m *Message) GetEtag() string {
	return m.Header.Etag
}

// SetWeakEtagVersion sets the etag version formatted as a weak Etag
func (m *Message) SetWeakEtagVersion(etag string) *Message {
	m.Header.Etag = "W/" + etag
	return m
}

// GetEtagVersion returns the version part of an E-tag.
// E.g. if Etag contains the weak etag 'W/"4232"' this function will return the string 4232
func (m *Message) GetEtagVersion() string {
	return ExtractEtagTypeVersion(m.Header.Etag)
}

// SetIfMatch sets the ifMatch header of the message
func (m *Message) SetIfMatch(ifMatch string) *Message {
	m.Header.IfMatch = ifMatch
	return m
}

// GetIfMatch returns the ifMatch header of the message
func (m *Message) GetIfMatch() string {
	return m.Header.IfMatch
}

// SetWeakIfMatchVersion sets the ifMatch header formatted as a weak Etag
func (m *Message) SetWeakIfMatchVersion(ifMatch string) *Message {
	m.Header.IfMatch = "W/" + ifMatch
	return m
}

// GetIfMatchVersion returns the version part of an If-Match header.
// E.g. if If-Match contains the weak etag 'W/"4232"' this function will return the string 4232
func (m *Message) GetIfMatchVersion() string {
	return ExtractEtagTypeVersion(m.Header.IfMatch)
}

// SetIfNoneExist sets the ifNoneExist header of the message
func (m *Message) SetIfNoneExist(ifNoneExist string) *Message {
	m.Header.IfNoneExist = ifNoneExist
	return m
}

// GetIfNoneExist returns the ifMatch header of the message
func (m *Message) GetIfNoneExist() string {
	return m.Header.IfNoneExist
}

// SetLocation sets the location header of the message
func (m *Message) SetLocation(location string) *Message {
	m.Header.Location = location
	return m
}

// GetLocation returns the location header of the message
func (m *Message) GetLocation() string {
	return m.Header.Location
}

// SetCorrelationID sets the correlationID of the message
func (m *Message) SetCorrelationID(correlationID string) *Message {
	m.Header.CorrelationID = correlationID
	return m
}

// GetCorrelationID returns the correlationID of the message
func (m *Message) GetCorrelationID() string {
	return m.Header.CorrelationID
}

// SetTransactionID sets the transactionID of the message
func (m *Message) SetTransactionID(transactionID string) *Message {
	m.Header.TransactionID = transactionID
	return m
}

// GetTransactionID returns the transactionID of the message
func (m *Message) GetTransactionID() string {
	return m.Header.TransactionID
}

// SetSecurity sets the security header of the message
func (m *Message) SetSecurity(security string) *Message {
	m.Header.Security = security
	return m
}

// GetSecurity returns the security header of the message
func (m *Message) GetSecurity() string {
	return m.Header.Security
}

// SetSession sets the session header of the message
func (m *Message) SetSession(session string) *Message {
	m.Header.Session = session
	return m
}

// GetSession returns the session header of the message
func (m *Message) GetSession() string {
	return m.Header.Session
}

// SetBody sets the body of the message
func (m *Message) SetBody(body string) *Message {
	m.Body = body
	return m
}

// GetBody returns the body of the message as a string
func (m *Message) GetBody() string {
	return m.Body
}

// ToByteArray returns message as ByteArray
func (m *Message) ToByteArray() []byte {
	message, err := json.Marshal(m)
	if err != nil {
		// do error stuff
	}
	return message
}

// ExtractEtagTypeVersion is a method...
func ExtractEtagTypeVersion(etag string) string {
	regex := regexp.MustCompile("W/\"(.*)\"")
	typeversion := regex.FindStringSubmatch(etag)
	if len(typeversion) < 2 {
		return etag
	}
	return typeversion[1]
}
