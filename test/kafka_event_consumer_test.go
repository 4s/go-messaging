package test

import (
	"context"
	"regexp"
	"testing"

	"bitbucket.org/4s/go-messaging/implementations/kafka"
	"bitbucket.org/4s/go-messaging/test/helpers"
)

func TestKafkaEventConsumer(t *testing.T) {

	err := helpers.ReadEnvFromFile("../kafka.env")
	if err != nil {
		t.Errorf("Failed to load environment variables from file: %v", err)
	}

	ctx := context.Background()
	regex := regexp.MustCompile("DataCreated_FHIR_Observation_(.*)")
	eventConsumer, err := kafka.NewEventConsumer(ctx, nil)
	if err != nil {
		t.Errorf("eventconsumer couldn't be created. Failed with error: %v", err)
	}

	eventConsumer.SubscribeTopicPattern(regex)
}
