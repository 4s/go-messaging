package test

import messaging "bitbucket.org/4s/go-messaging"

// MyEventConsumerInterceptorAdaptor is an implementation og EventConsumerAdaptor similar to EventConsumerInterceptorAdaptor, but testing purposes
type MyEventConsumerInterceptorAdaptor struct {
	IncomingMessagePreParsingCalled         bool
	IncomingMessagePreProcessMessageCalled  bool
	IncomingMessagePostProcessMessageCalled bool
	HandleErrorCalled                       bool
	ProcessingCompletedNormallyCalled       bool
}

// NewMyEventConsumerInterceptorAdaptor functions as a constructor for MyEventConsumerInterceptorAdaptor, returning a
// new instance of MyEventConsumerInterceptorAdaptor with all 'called'-variables set to false
func NewMyEventConsumerInterceptorAdaptor() MyEventConsumerInterceptorAdaptor {
	return MyEventConsumerInterceptorAdaptor{false, false, false, false, false}
}

// IncomingMessagePreParsing is a test implementation of EventConsumerInterceptor's IncomingMessagePreParsing
func (eventConsumerInterceptorAdaptor *MyEventConsumerInterceptorAdaptor) IncomingMessagePreParsing(consumedTopic messaging.Topic, receivedMessage messaging.Message) bool {
	eventConsumerInterceptorAdaptor.IncomingMessagePreParsingCalled = true
	return true
}

// IncomingMessagePreProcessMessage is a test implementation of EventConsumerInterceptor's IncomingMessagePreProcessMessage
func (eventConsumerInterceptorAdaptor *MyEventConsumerInterceptorAdaptor) IncomingMessagePreProcessMessage(consumedTopic messaging.Topic, receivedMessage messaging.Message) bool {
	eventConsumerInterceptorAdaptor.IncomingMessagePreProcessMessageCalled = true
	return true
}

// IncomingMessagePostProcessMessage is a test implementation of EventConsumerInterceptor's IncomingMessagePostProcessMessage
func (eventConsumerInterceptorAdaptor *MyEventConsumerInterceptorAdaptor) IncomingMessagePostProcessMessage(consumedTopic messaging.Topic, receivedMessage messaging.Message) bool {
	eventConsumerInterceptorAdaptor.IncomingMessagePostProcessMessageCalled = true
	return true
}

// HandleError is a test implementation of EventConsumerInterceptor's HandleError
func (eventConsumerInterceptorAdaptor *MyEventConsumerInterceptorAdaptor) HandleError(consumedTopic messaging.Topic, receivedMessage messaging.Message, err error) bool {
	eventConsumerInterceptorAdaptor.HandleErrorCalled = true
	return true
}

// ProcessingCompletedNormally is a test implementation of EventConsumerInterceptor's ProcessingCompletedNormally
func (eventConsumerInterceptorAdaptor *MyEventConsumerInterceptorAdaptor) ProcessingCompletedNormally(consumedTopic messaging.Topic, receivedMessage messaging.Message) {
	eventConsumerInterceptorAdaptor.ProcessingCompletedNormallyCalled = true
}
