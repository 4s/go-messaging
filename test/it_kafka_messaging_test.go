// +build integration

package test

import (
	"context"
	"encoding/json"
	"os"
	"os/signal"
	"regexp"
	"sync"
	"syscall"
	"testing"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"bitbucket.org/4s/go-messaging/test/helpers"
)

func TestConsumeAndProcessTopicList(t *testing.T) {
	var ctx = context.Background()

	err := helpers.ReadEnvFromFile("../kafka.env")
	if err != nil {
		t.Errorf("Failed to load environment variables from file: %v", err)
	}

	// Create topic
	topic := messaging.NewTopic()
	topic.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(ctx).Debugf(
		"topic set to: %v",
		topic.String(),
	)
	topics := messaging.Topics{}
	topics = append(topics, topic)

	// Create KafkaProducer
	testProducer, err := messaging.NewKafkaEventProducer(ctx, "Test producer")
	if err != nil {
		t.Errorf("could not create testProducer: %v", err)
	}

	// Produce test message on kafka
	message := messaging.NewMessage()
	message.SetBodyType("Text").
		SetContentVersion("3.5.0").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae").
		SetBody("Testbody")
	err = testProducer.Produce(topic, message)
	if err != nil {
		t.Errorf("could not produce with testProducer: %v", err)
	}

	// Create KafkaConsumer
	testConsumer, err := messaging.NewKafkaEventConsumer(ctx)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	testConsumer.SubscribeTopics(topics)

	eventProcessor, _ := NewMyEventProcessor(ctx)
	consumeAndProcess, err := messaging.NewKafkaConsumeAndProcess(
		ctx,
		&testConsumer,
		&testProducer,
		eventProcessor,
	)
	if err != nil {
		t.Errorf("could not create consumeAndProcess: %v", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go consumeAndProcess.Run(ctx, sigs, &wg)

	// Create KafkaConsumer
	verifyConsumer, err := messaging.NewKafkaEventConsumer(ctx)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	outputTopic := messaging.NewTopic()
	outputTopic.
		SetOperation(messaging.DataCreated).
		SetDataCategory(topic.GetDataCategory()).
		SetDataType(topic.GetDataType()).
		SetDataCodes(topic.GetDataCodes())
	outputTopics := messaging.Topics{}
	outputTopics = append(outputTopics, outputTopic)
	verifyConsumer.SubscribeTopics(outputTopics)

	// Poll messages from kafka for 10 seconds
	mess := messaging.Message{}
	var i int
	for start := time.Now(); ; {
		if i%10 == 0 {
			if time.Since(start) > time.Duration(10*time.Second) {
				break
			}
		}
		mess, _, err = verifyConsumer.Poll(ctx)
		if err != nil {
			t.Errorf("Error while polling from kafka: %v", err)
		}
		if mess != (messaging.Message{}) {
			break
		}
		i++
	}
	sigs <- os.Interrupt

	operationOutcome := messaging.NewOperationOutcome("information", "", "success")
	operationOutcomeJSON, err := json.Marshal(operationOutcome)
	if err != nil {
		t.Errorf("an error occurred while marshalling operationOutcome to JSON: %v", err)
	}

	if mess.GetBodyType() != "OperationOutcome" ||
		mess.GetContentVersion() != "3.5.0" ||
		mess.GetCorrelationID() != "wF1FdPFP1X8" ||
		mess.GetTransactionID() != "488f8476-fa5d-44e3-8f26-e185f08c3bae" ||
		mess.GetBody() != string(operationOutcomeJSON[:]) {
		t.Errorf(
			"Messages do not match \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n",
			mess.GetBodyType(), "OperationOutcome",
			mess.GetContentVersion(), "3.5.0",
			mess.GetCorrelationID(), "wF1FdPFP1X8",
			mess.GetTransactionID(), "488f8476-fa5d-44e3-8f26-e185f08c3bae",
			mess.GetBody(), string(operationOutcomeJSON[:]),
		)
	}
}

func TestConsumeAndProcessTopicPattern(t *testing.T) {
	var ctx = context.Background()

	os.Setenv("TESTING", "true")
	os.Setenv("KAFKA_GROUP_ID", "messaging-test")
	os.Setenv("KAFKA_BOOTSTRAP_SERVER", "kafka:9092")

	// Create topic
	topic := messaging.NewTopic()
	topic.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(ctx).Debugf(
		"topic set to: %v",
		topic.String(),
	)
	topics := messaging.Topics{}
	topics = append(topics, topic)

	// Create KafkaProducer
	testProducer, err := messaging.NewKafkaEventProducer(ctx, "Test producer")
	if err != nil {
		t.Errorf("could not create testProducer: %v", err)
	}

	// Produce test message on kafka
	message := messaging.NewMessage()
	message.SetBodyType("Text").
		SetContentVersion("3.5.0").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae").
		SetBody("Testbody")
	err = testProducer.Produce(topic, message)
	if err != nil {
		t.Errorf("could not produce with testProducer: %v", err)
	}

	// Create KafkaConsumer
	testConsumer, err := messaging.NewKafkaEventConsumer(ctx)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	regex := regexp.MustCompile("InputReceived_FHIR_QuestionnaireResponse(.*)")
	testConsumer.SubscribeTopicPattern(regex)

	eventProcessor, _ := NewMyEventProcessor(ctx)
	consumeAndProcess, err := messaging.NewKafkaConsumeAndProcess(
		ctx,
		&testConsumer,
		&testProducer,
		eventProcessor,
	)
	if err != nil {
		t.Errorf("could not create consumeAndProcess: %v", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go consumeAndProcess.Run(ctx, sigs, &wg)

	// Create KafkaConsumer
	verifyConsumer, err := messaging.NewKafkaEventConsumer(ctx)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	outputTopic := messaging.NewTopic()
	outputTopic.
		SetOperation(messaging.DataCreated).
		SetDataCategory(topic.GetDataCategory()).
		SetDataType(topic.GetDataType()).
		SetDataCodes(topic.GetDataCodes())
	outputTopics := messaging.Topics{}
	outputTopics = append(outputTopics, outputTopic)
	verifyConsumer.SubscribeTopics(outputTopics)

	// Poll messages from kafka for 10 seconds
	mess := messaging.Message{}
	var i int
	for start := time.Now(); ; {
		if i%10 == 0 {
			if time.Since(start) > time.Duration(10*time.Second) {
				break
			}
		}
		mess, _, err = verifyConsumer.Poll(ctx)
		if err != nil {
			t.Errorf("Error while polling from kafka: %v", err)
		}
		if mess != (messaging.Message{}) {
			break
		}
		i++
	}
	sigs <- os.Interrupt

	operationOutcome := messaging.NewOperationOutcome("information", "", "success")
	operationOutcomeJSON, err := json.Marshal(operationOutcome)
	if err != nil {
		t.Errorf("an error occurred while marshalling operationOutcome to JSON: %v", err)
	}

	if mess.GetBodyType() != "OperationOutcome" ||
		mess.GetContentVersion() != "3.5.0" ||
		mess.GetCorrelationID() != "wF1FdPFP1X8" ||
		mess.GetTransactionID() != "488f8476-fa5d-44e3-8f26-e185f08c3bae" ||
		mess.GetBody() != string(operationOutcomeJSON[:]) {
		t.Errorf(
			"Messages do not match \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n",
			mess.GetBodyType(), "OperationOutcome",
			mess.GetContentVersion(), "3.5.0",
			mess.GetCorrelationID(), "wF1FdPFP1X8",
			mess.GetTransactionID(), "488f8476-fa5d-44e3-8f26-e185f08c3bae",
			mess.GetBody(), string(operationOutcomeJSON[:]),
		)
	}
}
