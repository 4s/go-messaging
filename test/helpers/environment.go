package helpers

import (
	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

type Config struct {
	ServiceName          string `env:"SERVICE_NAME"`
	FhirVersion          string `env:"FHIR_VERSION"`
	KafkaBootstrapServer string `env:"KAFKA_BOOTSTRAO"`

	// KafkaConsumer
	KafkaEnableAutoCommit     string `env:"KAFKA_ENABLE_AUTO_COMMIT"`
	KafkaAutoCommitIntervalMS string `env:"KAFKA_AUTO_COMMIT_INTERVAL_MS"`
	KafkaSessionTimeoutMS     string `env:"KAFKA_SESSION_TIMEOUT_MS"`
	KafkaGroupID              string `env:"KAFKA_GROUP_ID"`

	// KafkaProducer
	KafkaAcks    string `env:"KAFKA_ACKS"`
	KafkaRetries string `env:"KAFKA_RETRIES"`

	// Consumer health
	EnableHealth     string `env:"ENABLE_HEALTH"`
	HealthFilePath   string `env:"HEALTH_FILE_PATH"`
	HealthIntervalMS string `env:"HEALTH_INTERVAL_MS"`
}

// ReadEnvFromFile reads environment variables from a .env file and sets them as environmentvariables on the host
func ReadEnvFromFile(path string) error {
	if err := godotenv.Load(path); err != nil {
		return err
	}
	conf := &Config{}
	err := env.Parse(conf)
	if err != nil {
		return err
	}
	return nil
}
