package test

import (
	"context"
	"encoding/json"
	"os"
	"os/signal"
	"regexp"
	"sync"
	"syscall"
	"testing"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"bitbucket.org/4s/go-messaging/implementations/mock"
	"bitbucket.org/4s/go-messaging/test/helpers"
)

func TestEventConsumerInterceptor(t *testing.T) {
	var ctx = context.Background()

	timeout := time.Duration(1 * time.Second)
	err := helpers.ReadEnvFromFile("../kafka.env")
	if err != nil {
		t.Errorf("Failed to load environment variables from file: %v", err)
	}

	// Create topic
	topic := messaging.NewTopic()
	topic.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(ctx).Debugf(
		"topic set to: %v",
		topic.String(),
	)
	topics := messaging.Topics{}
	topics = append(topics, topic)

	// Create KafkaProducer
	testProducer, err := mock.NewEventProducer(ctx, "Test producer")
	if err != nil {
		t.Errorf("could not create testProducer: %v", err)
	}

	// Produce test message on kafka
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae").
		SetBody("Testbody")
	err = testProducer.Produce(topic, message)
	if err != nil {
		t.Errorf("could not produce with testProducer: %v", err)
	}

	// Create KafkaConsumer
	eventProcessor, _ := NewMyEventProcessor(ctx)
	testConsumer, err := mock.NewEventConsumer(ctx, eventProcessor)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	testConsumer.SubscribeTopics(topics)

	consumeAndProcess, err := messaging.NewConsumeAndProcess(
		ctx,
		&testConsumer,
		&testProducer,
	)
	if err != nil {
		t.Errorf("could not create consumeAndProcess: %v", err)
	}

	eventConsumerInterceptorAdaptor := NewMyEventConsumerInterceptorAdaptor()
	consumeAndProcess.SetInterceptors(&eventConsumerInterceptorAdaptor)

	var wg sync.WaitGroup
	wg.Add(1)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go consumeAndProcess.Run(ctx, sigs, &wg)

	outputTopic := messaging.NewTopic()
	outputTopic.
		SetOperation(messaging.DataCreated).
		SetDataCategory(topic.GetDataCategory()).
		SetDataType(topic.GetDataType()).
		SetDataCodes(topic.GetDataCodes())

	// Poll messages from kafka
	future := testProducer.GetFutureTopic(ctx, outputTopic, timeout)
	mess := <-future
	sigs <- os.Interrupt

	if mess == (messaging.Message{}) {
		log.WithContext(ctx).Debug("Message is empty")
	}

	operationOutcome := messaging.NewOperationOutcome("information", "", "success")
	operationOutcomeJSON, err := json.Marshal(operationOutcome)
	if err != nil {
		t.Errorf("an error occurred while marshalling operationOutcome to JSON: %v", err)
	}
	if mess.GetBodyType() != "OperationOutcome" ||
		mess.GetContentVersion() != "3.5.0" ||
		mess.GetCorrelationID() != "wF1FdPFP1X8" ||
		mess.GetTransactionID() != "488f8476-fa5d-44e3-8f26-e185f08c3bae" ||
		mess.GetBody() != string(operationOutcomeJSON[:]) {
		t.Errorf(
			"Messages do not match \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n",
			mess.GetBodyType(), "OperationOutcome",
			mess.GetContentVersion(), "3.5.0",
			mess.GetCorrelationID(), "wF1FdPFP1X8",
			mess.GetTransactionID(), "488f8476-fa5d-44e3-8f26-e185f08c3bae",
			mess.GetBody(), string(operationOutcomeJSON[:]),
		)
	}

	if !eventConsumerInterceptorAdaptor.IncomingMessagePreParsingCalled {
		t.Errorf(
			"EventConsumer does not work as expected. \n"+
				"eventConsumerInterceptorAdaptor.IncomingMessagePreParsingCalled: %v \n",
			eventConsumerInterceptorAdaptor.IncomingMessagePreParsingCalled,
		)
	}
	if !eventConsumerInterceptorAdaptor.IncomingMessagePreProcessMessageCalled {
		t.Errorf(
			"EventConsumer does not work as expected. \n"+
				"eventConsumerInterceptorAdaptor.IncomingMessagePreProcessMessageCalled: %v \n",
			eventConsumerInterceptorAdaptor.IncomingMessagePreProcessMessageCalled,
		)
	}
	if !eventConsumerInterceptorAdaptor.IncomingMessagePostProcessMessageCalled {
		t.Errorf(
			"EventConsumer does not work as expected. \n"+
				"eventConsumerInterceptorAdaptor.IncomingMessagePostProcessMessageCalled: %v \n",
			eventConsumerInterceptorAdaptor.IncomingMessagePostProcessMessageCalled,
		)
	}
	if !eventConsumerInterceptorAdaptor.ProcessingCompletedNormallyCalled {
		t.Errorf(
			"EventConsumer does not work as expected. \n"+
				"eventConsumerInterceptorAdaptor.ProcessingCompletedNormallyCalled: %v \n",
			eventConsumerInterceptorAdaptor.ProcessingCompletedNormallyCalled,
		)
	}
}

func TestConsumeAndProcessTopicListMock(t *testing.T) {
	var ctx = context.Background()

	timeout := time.Duration(1 * time.Second)
	err := helpers.ReadEnvFromFile("../kafka.env")
	if err != nil {
		t.Errorf("Failed to load environment variables from file: %v", err)
	}

	// Create topic
	topic := messaging.NewTopic()
	topic.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(ctx).Debugf(
		"topic set to: %v",
		topic.String(),
	)
	topics := messaging.Topics{}
	topics = append(topics, topic)

	// Create KafkaProducer
	testProducer, err := mock.NewEventProducer(ctx, "Test producer")
	if err != nil {
		t.Errorf("could not create testProducer: %v", err)
	}

	// Produce test message on kafka
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae").
		SetBody("Testbody")
	err = testProducer.Produce(topic, message)
	if err != nil {
		t.Errorf("could not produce with testProducer: %v", err)
	}

	// Create KafkaConsumer
	eventProcessor, _ := NewMyEventProcessor(ctx)
	testConsumer, err := mock.NewEventConsumer(ctx, eventProcessor)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	testConsumer.SubscribeTopics(topics)

	consumeAndProcess, err := messaging.NewConsumeAndProcess(
		ctx,
		&testConsumer,
		&testProducer,
	)
	if err != nil {
		t.Errorf("could not create consumeAndProcess: %v", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go consumeAndProcess.Run(ctx, sigs, &wg)

	outputTopic := messaging.NewTopic()
	outputTopic.
		SetOperation(messaging.DataCreated).
		SetDataCategory(topic.GetDataCategory()).
		SetDataType(topic.GetDataType()).
		SetDataCodes(topic.GetDataCodes())

	// Poll messages from kafka
	future := testProducer.GetFutureTopic(ctx, outputTopic, timeout)
	mess := <-future
	sigs <- os.Interrupt

	if mess == (messaging.Message{}) {
		log.WithContext(ctx).Debug("Message is empty")
	}

	operationOutcome := messaging.NewOperationOutcome("information", "", "success")
	operationOutcomeJSON, err := json.Marshal(operationOutcome)
	if err != nil {
		t.Errorf("an error occurred while marshalling operationOutcome to JSON: %v", err)
	}
	if mess.GetBodyType() != "OperationOutcome" ||
		mess.GetContentVersion() != "3.5.0" ||
		mess.GetCorrelationID() != "wF1FdPFP1X8" ||
		mess.GetTransactionID() != "488f8476-fa5d-44e3-8f26-e185f08c3bae" ||
		mess.GetBody() != string(operationOutcomeJSON[:]) {
		t.Errorf(
			"Messages do not match \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n",
			mess.GetBodyType(), "OperationOutcome",
			mess.GetContentVersion(), "3.5.0",
			mess.GetCorrelationID(), "wF1FdPFP1X8",
			mess.GetTransactionID(), "488f8476-fa5d-44e3-8f26-e185f08c3bae",
			mess.GetBody(), string(operationOutcomeJSON[:]),
		)
	}
}

func TestConsumeAndProcessTopicPatternMock(t *testing.T) {
	var ctx = context.Background()

	timeout := time.Duration(1 * time.Second)

	os.Setenv("TESTING", "true")
	os.Setenv("KAFKA_GROUP_ID", "messaging-test")
	os.Setenv("KAFKA_BOOTSTRAP_SERVER", "kafka:9092")

	// Create topic
	topic := messaging.NewTopic()
	topic.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("QuestionnaireResponse")
	log.WithContext(ctx).Debugf(
		"topic set to: %v",
		topic.String(),
	)
	topics := messaging.Topics{}
	topics = append(topics, topic)

	// Create KafkaProducer
	testProducer, err := mock.NewEventProducer(ctx, "Test producer")
	if err != nil {
		t.Errorf("could not create testProducer: %v", err)
	}

	// Produce test message on kafka
	message := messaging.NewMessage()
	message.SetBodyType("FHIR").
		SetContentVersion("R4").
		SetCorrelationID("wF1FdPFP1X8").
		SetTransactionID("488f8476-fa5d-44e3-8f26-e185f08c3bae").
		SetBody("Testbody")
	err = testProducer.Produce(topic, message)
	if err != nil {
		t.Errorf("could not produce with testProducer: %v", err)
	}

	// Create KafkaConsumer
	eventProcessor, _ := NewMyEventProcessor(ctx)
	testConsumer, err := mock.NewEventConsumer(ctx, eventProcessor)
	if err != nil {
		t.Errorf("could not create testConsumer: %v", err)
	}
	regex := regexp.MustCompile("InputReceived_FHIR_QuestionnaireResponse(.*)")
	testConsumer.SubscribeTopicPattern(regex)

	consumeAndProcess, err := messaging.NewConsumeAndProcess(
		ctx,
		&testConsumer,
		&testProducer,
	)
	if err != nil {
		t.Errorf("could not create consumeAndProcess: %v", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go consumeAndProcess.Run(ctx, sigs, &wg)

	outputTopic := messaging.NewTopic()
	outputTopic.
		SetOperation(messaging.DataCreated).
		SetDataCategory(topic.GetDataCategory()).
		SetDataType(topic.GetDataType()).
		SetDataCodes(topic.GetDataCodes())

	// Poll messages from kafka
	future := testProducer.GetFutureTopic(ctx, outputTopic, timeout)
	mess := <-future
	sigs <- os.Interrupt

	if mess == (messaging.Message{}) {
		log.WithContext(ctx).Debug("Message is empty")
	}

	operationOutcome := messaging.NewOperationOutcome("information", "", "success")
	operationOutcomeJSON, err := json.Marshal(operationOutcome)
	if err != nil {
		t.Errorf("an error occurred while marshalling operationOutcome to JSON: %v", err)
	}
	if mess.GetBodyType() != "OperationOutcome" ||
		mess.GetContentVersion() != "3.5.0" ||
		mess.GetCorrelationID() != "wF1FdPFP1X8" ||
		mess.GetTransactionID() != "488f8476-fa5d-44e3-8f26-e185f08c3bae" ||
		mess.GetBody() != string(operationOutcomeJSON[:]) {
		t.Errorf(
			"Messages do not match \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n"+
				"have: %v, want: %v \n",
			mess.GetBodyType(), "OperationOutcome",
			mess.GetContentVersion(), "3.5.0",
			mess.GetCorrelationID(), "wF1FdPFP1X8",
			mess.GetTransactionID(), "488f8476-fa5d-44e3-8f26-e185f08c3bae",
			mess.GetBody(), string(operationOutcomeJSON[:]),
		)
	}
}
