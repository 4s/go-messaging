package test

import (
	"testing"

	"bitbucket.org/4s/go-messaging"
)

func TestTopicParser(t *testing.T) {
	topic1 := messaging.NewTopic()
	topic1.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("Observation").
		AddDataCode("MDC188736")
	topic2 := messaging.NewTopic()
	topic2.SetOperation(messaging.InputReceived).
		SetDataCategory(messaging.FHIR).
		SetDataType("Bundle").
		AddDataCode("MDC188736").
		AddDataCode("MDC150020").
		AddDataCode("MDC149546")
	topic3 := messaging.NewTopic()
	topic3.SetOperation(messaging.Create).
		SetDataCategory(messaging.FHIR).
		SetDataType("Questionnaire")

	tests := map[string]struct {
		input  string
		output messaging.Topic
	}{
		"InputReceived_FHIR_Observation_MDC188736": {
			input:  "InputReceived_FHIR_Observation_MDC188736",
			output: topic1,
		},
		"InputReceived_FHIR_Bundle_MDC188736_MDC150020_MDC149546": {
			input:  "InputReceived_FHIR_Bundle_MDC188736_MDC150020_MDC149546",
			output: topic2,
		},
		"Create_FHIR_Questionnaire": {
			input:  "Create_FHIR_Questionnaire",
			output: topic3,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		topic, err := messaging.TopicFromString(test.input)
		if err != nil {
			t.Errorf("parsing topic failed with error: %v", err)
		}

		if !test.output.Equals(topic) {
			t.Errorf(
				"parsing topic failed \n"+
					"have: %v, want: %v \n"+
					"have: %v, want: %v \n"+
					"have: %v, want: %v \n"+
					"have: %v, want: %v \n",
				topic.GetOperation(), test.output.GetOperation(),
				topic.GetDataCategory(), test.output.GetDataCategory(),
				topic.GetDataType(), test.output.GetDataType(),
				topic.GetDataCodes(), test.output.GetDataCodes(),
			)
		}
	}
}
