package test

import (
	"context"
	"testing"

	"bitbucket.org/4s/go-messaging"

	"bitbucket.org/4s/go-messaging/implementations/kafka"
	"bitbucket.org/4s/go-messaging/test/helpers"
)

func TestParse(t *testing.T) {
	messagePath := "./testdata/message.json"
	ctx := context.Background()

	rawMessage, err := helpers.GetJSON(messagePath)
	if err != nil {
		t.Errorf("could not read JSON from file: %v", err)
	}

	message, err := kafka.ParseMessage(ctx, rawMessage)
	if err != nil {
		t.Errorf("could not unmarshall JSON from file: %v", err)
	}

	if message.GetSender() == "" {
		t.Errorf("sender-field is empty")
	}
	if message.GetBodyCategory() != messaging.FHIR {
		t.Errorf("bodyCategory-field is incorrect")
	}
	if message.GetBodyType() == "" {
		t.Errorf("bodyType-field is empty")
	}
	if message.GetContentVersion() == "" {
		t.Errorf("contentVersion-field is empty")
	}
	if message.GetPrefer() != messaging.REPRESENTATION {
		t.Errorf("prefer-field is incorrect")
	}
	if message.GetEtag() == "" {
		t.Errorf("etag-field is empty")
	}
	if message.GetIfMatch() == "" {
		t.Errorf("ifMatch-field is empty")
	}
	if message.GetIfNoneExist() == "" {
		t.Errorf("ifNoneExist-field is empty")
	}
	if message.GetLocation() == "" {
		t.Errorf("location-field is empty")
	}
	if message.GetCorrelationID() == "" {
		t.Errorf("correlationId-field is empty")
	}
	if message.GetTransactionID() == "" {
		t.Errorf("transactionId-field is empty")
	}
	if message.GetSecurity() == "" {
		t.Errorf("security-field is empty")
	}
	if message.GetSession() == "" {
		t.Errorf("session-field is empty")
	}

	if message.GetBody() == "" {
		t.Errorf("body-field is empty")
	}
}
