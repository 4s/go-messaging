package test

import (
	"errors"
	"testing"

	messaging "bitbucket.org/4s/go-messaging"
)

func TestPreferFromString(t *testing.T) {
	tests := map[string]struct {
		input  string
		output messaging.Prefer
		err    error
	}{
		"Representation": {
			input:  "return=representation",
			output: messaging.REPRESENTATION,
			err:    nil,
		},
		"Minimal": {
			input:  "return=minimal",
			output: messaging.MINIMAL,
			err:    nil,
		},
		"OperationOutcome": {
			input:  "return=OperationOutcome",
			output: messaging.OPERATIONOUTCOME,
			err:    nil,
		},
		"Malformed: \"return=\"": {
			input:  "return=",
			output: 0,
			err:    errors.New("Unknown prefer"),
		},
		"Malformed: \"=\"": {
			input:  "=",
			output: 0,
			err:    errors.New("malformed prefer: missing \"return\""),
		},
		"Malformed: \" \"": {
			input:  " ",
			output: 0,
			err:    errors.New("malformed prefer: missing \"=\""),
		},
		"Malformed: \"\"": {
			input:  "",
			output: 0,
			err:    errors.New("prefer empty"),
		},
		"Unknown: \"foo=bar\"": {
			input:  "foo=bar",
			output: 0,
			err:    errors.New("malformed prefer: missing \"return\""),
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		result, err := messaging.PreferFromString(test.input)
		if err != nil && err.Error() != test.err.Error() {
			t.Errorf("PreferFromString failed with error: %v", err)
		}
		if result != test.output {
			t.Errorf(
				"PreferFromString failed: \n"+
					"have: %v, want %v",
				result, test.output,
			)
		}
	}
}

func TestGetEtagVersion(t *testing.T) {
	tests := map[string]struct {
		input  string
		output string
	}{
		"Malformed: \"\"": {
			input:  "",
			output: "",
		},
		"Input: \"W/\"4232\"\"": {
			input:  "W/\"4232\"",
			output: "4232",
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		m := messaging.NewMessage()
		m.SetEtag(test.input)
		result := m.GetEtagVersion()
		if result != test.output {
			t.Errorf(
				"GetEtagVersion failed: \n"+
					"have: %v, want %v",
				result, test.output,
			)
		}
	}
}

func TestIfMatchVersion(t *testing.T) {
	tests := map[string]struct {
		input  string
		output string
	}{
		"Malformed: \"\"": {
			input:  "",
			output: "",
		},
		"Input: \"W/\"4232\"\"": {
			input:  "W/\"4232\"",
			output: "4232",
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		m := messaging.NewMessage()
		m.SetIfMatch(test.input)
		result := m.GetIfMatchVersion()
		if result != test.output {
			t.Errorf(
				"GetIfMatchVersion failed: \n"+
					"have: %v, want %v",
				result, test.output,
			)
		}
	}
}
