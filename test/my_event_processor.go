package test

import (
	"context"
	"encoding/json"
	"errors"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
)

// MyEventProcessor is a struct representing MyEventProcessor which processes events received from
// Kafka, and returns a new message based on this processing
type MyEventProcessor struct{}

// NewMyEventProcessor functions as a constructor for MyEventProcessor that returns a new instance of MyEventProcessor
func NewMyEventProcessor(ctx context.Context) (MyEventProcessor, error) {
	return MyEventProcessor{}, nil
}

// ProcessMessage accepts messages from Kafka and outputs an OperationOutcome resource to a different Topic
func (mep MyEventProcessor) ProcessMessage(
	ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message,
) error {
	log.WithContext(ctx).Debugf("Processing incoming resource on topic: %s", consumedTopic.String())
	messageProcessedTopic.
		SetOperation(messaging.DataCreated).
		SetDataCategory(consumedTopic.GetDataCategory()).
		SetDataType(consumedTopic.GetDataType()).
		SetDataCodes(consumedTopic.GetDataCodes())

	operationOutcome := messaging.NewOperationOutcome("information", "", "success")
	operationOutcomeJSON, err := json.Marshal(operationOutcome)
	if err != nil {
		return errors.New("an error occurred while marshalling operationOutcome to JSON: " + err.Error())
	}
	outgoingMessage.
		SetBody(string(operationOutcomeJSON[:])).
		SetBodyCategory(messaging.FHIR).
		SetBodyType("OperationOutcome").
		SetContentVersion(messaging.GetEnv(ctx, "FHIR_VERSION", "3.5.0")).
		SetCorrelationID(receivedMessage.GetCorrelationID()).
		SetTransactionID(receivedMessage.GetTransactionID())
	return nil
}
