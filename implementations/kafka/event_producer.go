package kafka

import (
	"context"
	"errors"

	log "bitbucket.org/4s/go-logging"
	"bitbucket.org/4s/go-messaging"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// EventProducer is a struct representing an event producer for Kafka messages
type EventProducer struct {
	producer *kafka.Producer
	sender   string
}

// NewEventProducer functions as a constructor for EventProducer that returns a new instance of EventProducer
func NewEventProducer(ctx context.Context, sender string) (EventProducer, error) {

	// Kafka configuration
	bootstrapServer := messaging.GetEnv(ctx, "KAFKA_BOOTSTRAP_SERVER", "")
	if bootstrapServer == "" {
		return EventProducer{}, errors.New("KAFKA_BOOTSTRAP_SERVER is not defined")
	}
	acks := messaging.GetEnv(ctx, "KAFKA_ACKS", "all")
	retries := messaging.GetEnv(ctx, "KAFKA_RETRIES", "0")

	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": bootstrapServer,
		"acks":              acks,
		"retries":           retries,
	})
	if err != nil {
		return EventProducer{}, err
	}

	eventProducer := EventProducer{
		producer,
		sender,
	}

	log.WithContext(ctx).Debugf(
		"created Kafka producer with bootstrap server: %v, acks: %v and retries: %v",
		bootstrapServer, acks, retries,
	)

	return eventProducer, nil
}

// Produce produces message on the topic set for the EventProducer
func (eventProducer *EventProducer) Produce(topic messaging.Topic, message messaging.Message) error {
	deliveryChan := make(chan kafka.Event)

	message.SetSender(eventProducer.sender)

	topicString := topic.String()
	kafkaMessage := &kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topicString, Partition: kafka.PartitionAny},
		Value:          message.ToByteArray(),
	}

	err := eventProducer.producer.Produce(kafkaMessage, deliveryChan)
	if err != nil {
		return err
	}

	// Send message
	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		return errors.New("delivery failed: " + m.TopicPartition.Error.Error())
	}

	close(deliveryChan)
	return nil
}

// Close closes EventProducer
func (eventProducer *EventProducer) Close() {
	eventProducer.producer.Close()
}
