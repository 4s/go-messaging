package kafka

import (
	"context"
	"errors"
	"regexp"

	"bitbucket.org/4s/go-messaging"

	log "bitbucket.org/4s/go-logging"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// EventConsumer is a struct for representing an event consumer for Kafka messages
type EventConsumer struct {
	consumer        *kafka.Consumer
	processor       messaging.EventProcessor
	topics          *messaging.Topics
	topicPattern    *regexp.Regexp
	PreParseHook    func(topic messaging.Topic, message messaging.Message) bool
	PreProcessHook  func(topic messaging.Topic, message messaging.Message) bool
	PostProcessHook func(topic messaging.Topic, message messaging.Message) bool
}

// NewEventConsumer functions as a constructor for EventConsumer that returns a new instance of EventConsumer
func NewEventConsumer(ctx context.Context, processor messaging.EventProcessor) (EventConsumer, error) {

	// Kafka configuration
	bootstrapServer := messaging.GetEnv(ctx, "KAFKA_BOOTSTRAP_SERVER", "")
	groupID := messaging.GetEnv(ctx, "KAFKA_GROUP_ID", "")
	if bootstrapServer == "" {
		return EventConsumer{}, errors.New("KAFKA_BOOTSTRAP_SERVER is not defined")
	}
	if groupID == "" {
		return EventConsumer{}, errors.New("KAFKA_GROUP_ID not set")
	}
	sessionTimeout := messaging.GetEnv(ctx, "KAFKA_SESSION_TIMEOUT_MS", "10000")
	enableAutoCommit := messaging.GetEnv(ctx, "KAFKA_ENABLE_AUTO_COMMIT", "false")
	autoCommitIntervalMS := messaging.GetEnv(ctx, "KAFKA_AUTO_COMMIT_INTERVAL", "1000")

	// Create new consumer
	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":       bootstrapServer,
		"group.id":                groupID,
		"session.timeout.ms":      sessionTimeout,
		"default.topic.config":    kafka.ConfigMap{"auto.offset.reset": "earliest"},
		"enable.auto.commit":      enableAutoCommit,
		"auto.commit.interval.ms": autoCommitIntervalMS,
		"metadata.max.age.ms":     5000,
		"auto.offset.reset":       "earliest",
	})

	if err != nil {
		return EventConsumer{}, err
	}

	log.WithContext(ctx).Debug("created EventConsumer with bootstrap server: " + bootstrapServer +
		", group id: " + groupID +
		" and sessionTimeout: " + sessionTimeout)

	eventConsumer := EventConsumer{
		consumer,
		processor,
		nil,
		nil,
		nil,
		nil,
		nil,
	}

	return eventConsumer, nil
}

// SubscribeTopics subscribes the EventConsumer to topics based on provided string-slice
// Furthermore SubscribeTopics support regex for topics prefixed with ^
// example: "^my.*pattern.."
func (eventConsumer *EventConsumer) SubscribeTopics(topics messaging.Topics) error {
	eventConsumer.topics = &topics

	err := eventConsumer.consumer.SubscribeTopics(topics.StringArray(), nil)
	if err != nil {
		return err
	}

	return nil
}

// SubscribeTopicPattern subscribes the EventConsumer to all topics matching the provided regular expression
func (eventConsumer *EventConsumer) SubscribeTopicPattern(regexp *regexp.Regexp) error {
	eventConsumer.topicPattern = regexp

	topicPatternArray := []string{}
	topicPatternArray = append(topicPatternArray, "^"+regexp.String())

	err := eventConsumer.consumer.SubscribeTopics(topicPatternArray, nil)
	if err != nil {
		return err
	}

	return nil
}

// Poll polls the topics subscribed to by the EventConsumer
func (eventConsumer *EventConsumer) Poll(ctx context.Context) (messaging.Message, messaging.Topic, error) {
	var message messaging.Message
	var topic messaging.Topic
	if (eventConsumer.topics == nil || len(*eventConsumer.topics) == 0) && (eventConsumer.topicPattern == nil) {
		return message, topic, errors.New("Consumer has not subscribed to any topics")
	}

	event := eventConsumer.consumer.Poll(100)
	if event == nil {
		return message, topic, nil
	}

	switch e := event.(type) {
	case *kafka.Message:
		var err error
		topic, err = messaging.TopicFromString(*e.TopicPartition.Topic)
		if err != nil {
			return message, topic, err
		}
		if eventConsumer.PreParseHook != nil {
			continueProcessing := eventConsumer.PreParseHook(topic, message)
			if !continueProcessing {
				return messaging.Message{}, messaging.Topic{}, errors.New("PreParseHook returned false, not continuing processing")
			}
		}
		message, err = ParseMessage(ctx, e.Value)
		if err != nil {
			log.WithContext(ctx).Debug(err.Error())
			return message, topic, err
		}
	case kafka.PartitionEOF:
		log.WithContext(ctx).Debug("reached " + e.String())
	case kafka.Error:
		return message, topic, errors.New("error: " + e.String())
	default:
		return message, topic, nil
	}
	return message, topic, nil
}

// ProcessMessage is...
func (eventConsumer *EventConsumer) ProcessMessage(ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message) error {

	if eventConsumer.PreProcessHook != nil {
		continueProcessing := eventConsumer.PreProcessHook(consumedTopic, receivedMessage)
		if !continueProcessing {
			return errors.New("PreProcessHook returned false, not continuing processing")
		}
	}

	eventConsumer.processor.ProcessMessage(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage)

	if eventConsumer.PostProcessHook != nil {
		continueProcessing := eventConsumer.PostProcessHook(consumedTopic, receivedMessage)
		if !continueProcessing {
			return errors.New("PostProcessHook returned false, not continuing processing")
		}
	}

	return nil
}

// Commit commits offset for currently assigned partitions
// This is a blocking call
func (eventConsumer *EventConsumer) Commit() error {
	_, err := eventConsumer.consumer.Commit()
	return err
}

// SetPreParseHook is used to set a method that should be called before an incoming message is parsed
func (eventConsumer *EventConsumer) SetPreParseHook(preParseHook func(topic messaging.Topic, message messaging.Message) bool) {
	eventConsumer.PreParseHook = preParseHook
}

// SetPreProcessHook is used to set a method that should be called before an incoming message is processed
func (eventConsumer *EventConsumer) SetPreProcessHook(preProcessHook func(topic messaging.Topic, message messaging.Message) bool) {
	eventConsumer.PreProcessHook = preProcessHook
}

// SetPostProcessHook is used to set a method that should be called after an incoming message is processed
func (eventConsumer *EventConsumer) SetPostProcessHook(postProcessHook func(topic messaging.Topic, message messaging.Message) bool) {
	eventConsumer.PostProcessHook = postProcessHook
}

// Close closes the EventConsumer
func (eventConsumer *EventConsumer) Close() {
	eventConsumer.consumer.Close()
}
