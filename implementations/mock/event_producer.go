package mock

import (
	"context"
	"encoding/json"
	"time"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// EventProducer is a struct for representing an event producer Mock implementation
type EventProducer struct {
	store  *Datastore
	sender string
}

// NewEventProducer functions as a constructor for EventProducer that returns a new instance of EventProducer
func NewEventProducer(ctx context.Context, sender string) (EventProducer, error) {
	log.WithContext(ctx).Debugf(
		"creating new EventProducer with sender: %v,",
		sender,
	)
	return EventProducer{
		GetDatastore(),
		sender,
	}, nil
}

// Produce converts messages to kafka messages and saves them to the mock datastore
func (eventProducer *EventProducer) Produce(topic messaging.Topic, message messaging.Message) error {

	topicString := topic.String()
	kafkaMessage := &kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topicString, Partition: kafka.PartitionAny},
		Value:          message.ToByteArray(),
	}
	err := eventProducer.store.Save(*kafkaMessage)
	if err != nil {
		return err
	}
	return nil
}

// GetFutureTopic returns a chan functioning as a future that resolves to a message, when one is available on the
// specified topic (if the timeout does not occur first)
func (eventProducer *EventProducer) GetFutureTopic(
	ctx context.Context,
	topic messaging.Topic,
	timeout time.Duration,
) chan messaging.Message {
	future := make(chan messaging.Message)
	go func() { future <- eventProducer.waitForOutput(ctx, topic, timeout) }()
	return future
}

func (eventProducer *EventProducer) waitForOutput(
	ctx context.Context,
	topic messaging.Topic,
	timeout time.Duration,
) messaging.Message {
	var message messaging.Message
	var i int
	for start := time.Now(); ; {
		if i%10 == 0 {
			if time.Since(start) > timeout {
				break
			}
		}
		kafkaMessage, err := eventProducer.store.Retrieve(ctx, topic.String())
		if err != nil {
			return messaging.Message{}
		}
		if kafkaMessage != nil {
			var message messaging.Message
			err := json.Unmarshal(kafkaMessage.Value, &message)
			if err != nil {
				log.WithContext(ctx).Debug(err.Error())
			}
			return message
		}
		i++
	}
	return message
}

// Close does nothing and is only implemented to implement the interface
func (eventProducer *EventProducer) Close() {
	// do nothing
}
