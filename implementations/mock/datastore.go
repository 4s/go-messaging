package mock

import (
	"context"
	"errors"
	"regexp"
	"sync"

	"github.com/confluentinc/confluent-kafka-go/kafka"

	log "bitbucket.org/4s/go-logging"
)

// Datastore is a data storage struct that is used to mock the Kafka message queue
type Datastore struct {
	store map[string][]kafka.Message
}

var instance *Datastore
var mu sync.Mutex

// GetDatastore returns either an existing instance of the Datastore, if one exists, or a new one.
func GetDatastore() *Datastore {
	if instance == nil { // <-- Not yet perfect. since it's not fully atomic
		mu.Lock()
		defer mu.Unlock()

		if instance == nil {
			mockDatastore := newDatastore()
			instance = &mockDatastore
		}
	}
	return instance
}

// NewDatastore functions as a constructor for Datastore that returns a new instance of Datastore
func newDatastore() Datastore {
	return Datastore{
		map[string][]kafka.Message{},
	}
}

// Save saves the provided kafka.Message to the datastore under the topic provided in the topicpartition in the message
func (datastore *Datastore) Save(message kafka.Message) error {
	mu.Lock()
	defer mu.Unlock()
	if message.TopicPartition == (kafka.TopicPartition{}) {
		return errors.New("message topic partition is empty")
	}
	if message.TopicPartition.Topic == nil || *message.TopicPartition.Topic == "" {
		return errors.New("message has no topic")
	}

	var messages []kafka.Message
	if val, ok := datastore.store[*message.TopicPartition.Topic]; ok {
		messages = val
	}
	messages = append(messages, message)
	log.WithContext(context.Background()).Debugf(
		"saving kafka message to topic: %v",
		*message.TopicPartition.Topic,
	)
	datastore.store[*message.TopicPartition.Topic] = messages

	return nil
}

// Retrieve retrieves the oldest kafka.Message stored under a specific topic from the Datastore
func (datastore *Datastore) Retrieve(ctx context.Context, topic string) (*kafka.Message, error) {
	mu.Lock()
	defer mu.Unlock()
	if topic == "" {
		return nil, errors.New("topic is empty")
	}

	if _, ok := datastore.store[topic]; !ok {
		return nil, nil
	}
	if len(datastore.store[topic]) == 0 {
		return nil, nil
	}

	log.WithContext(ctx).Debugf(
		"retrieving kafka message from topic: %v",
		topic,
	)
	message := datastore.store[topic][0]
	datastore.store[topic] = datastore.store[topic][1:]
	return &message, nil
}

// RetrieveFromPattern retrieves the oldest kafka.Message stored under any topic matching the regexp provided from the
// Datastore
func (datastore *Datastore) RetrieveFromPattern(ctx context.Context, regexp *regexp.Regexp) (*kafka.Message, error) {
	mu.Lock()
	defer mu.Unlock()

	var matchTopic string
	for topic := range datastore.store {
		if regexp.MatchString(topic) {
			matchTopic = topic
		}
	}
	if matchTopic == "" {
		return nil, nil
	}
	if len(datastore.store[matchTopic]) == 0 {
		return nil, nil
	}

	log.WithContext(ctx).Debugf(
		"retrieving kafka message from topic: %v",
		matchTopic,
	)
	message := datastore.store[matchTopic][0]
	datastore.store[matchTopic] = datastore.store[matchTopic][1:]
	return &message, nil
}
