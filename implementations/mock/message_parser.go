package mock

import (
	"context"
	"encoding/json"
	"errors"

	log "bitbucket.org/4s/go-logging"
	"bitbucket.org/4s/go-messaging"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// ParseMessage parses a message on the form below and return a corresponding Message object:
//
// {
// 		"header": {
//			"sender": "obs-input-service",
//      "bodyCategory": "FHIR",
//      "bodyType": "Observation",
//      "contentVersion": "3.3.0",
//      "prefer": "return=representation",
//      "etag": "W/\"4232\"",
//      "location":"http://fhirtest.uhn.ca/baseDstu3/Observation/15354/_history/1"
//      "correlationId": "xez5ZXQcDG6"
//      "transactionId": "e6651fe2-fb8e-4a54-8b8e-7343dbdb997c"
//      "security": ...
//  },
//  "body":
//  {
//       "resourceType": "Observation",
//        ...
//   }
// }
func ParseMessage(ctx context.Context, kafkaMessage *kafka.Message) (messaging.Message, error) {
	var message messaging.Message
	err := json.Unmarshal(kafkaMessage.Value, &message)
	if err != nil {
		return message, err
	}
	if message.GetHeaderVersion() == (messaging.Version{}) {
		return messaging.Message{}, errors.New("no headerVersion in header")
	}
	if !message.GetHeaderVersion().IsCompatible(messaging.VERSION) {
		return messaging.Message{}, errors.New("Version in message header incompatible with library " +
			"version. Message header version = " + message.GetHeaderVersion().String() +
			". Library version = " + messaging.VERSION.String())
	}
	if message.GetSender() == "" {
		log.WithContext(ctx).Warn("No sender in header")
	}
	if message.GetBodyType() == "" {
		log.WithContext(ctx).Warn("No bodyType in header")
	}
	if message.GetContentVersion() == "" {
		log.WithContext(ctx).Warn("No contentVersion in header")
	}
	if message.GetEtag() == "" {
		log.WithContext(ctx).Warn("No etag in header")
	}
	if message.GetIfMatch() == "" {
		log.WithContext(ctx).Warn("No ifMatch in header")
	}
	if message.GetIfNoneExist() == "" {
		log.WithContext(ctx).Warn("No ifNoneExist in header")
	}
	if message.GetLocation() == "" {
		log.WithContext(ctx).Warn("No location in header")
	}
	if message.GetCorrelationID() == "" {
		log.WithContext(ctx).Warn("No correlationId in header. One will be generated")
		message.SetCorrelationID(messaging.VerifyOrCreateID(""))
	}
	if message.GetTransactionID() == "" {
		log.WithContext(ctx).Warn("No transactionId in header")
	}
	if message.GetSecurity() == "" {
		log.WithContext(ctx).Warn("No security in header")
	}

	return message, err
}
