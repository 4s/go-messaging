package mock

import (
	"context"
	"encoding/json"
	"errors"
	"regexp"

	log "bitbucket.org/4s/go-logging"
	messaging "bitbucket.org/4s/go-messaging"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// EventConsumer is a struct for representing an event consumer Mock implementation
type EventConsumer struct {
	processor       messaging.EventProcessor
	store           *Datastore
	topics          *messaging.Topics
	topicPattern    *regexp.Regexp
	PreParseHook    func(topic messaging.Topic, message messaging.Message) bool
	PreProcessHook  func(topic messaging.Topic, message messaging.Message) bool
	PostProcessHook func(topic messaging.Topic, message messaging.Message) bool
}

// NewEventConsumer functions as a constructor for EventConsumer that returns a new instance of EventConsumer
func NewEventConsumer(ctx context.Context, processor messaging.EventProcessor) (EventConsumer, error) {
	return EventConsumer{
		processor,
		GetDatastore(),
		nil,
		nil,
		nil,
		nil,
		nil,
	}, nil
}

// SubscribeTopics subscribes the EventConsumer to topics based on provided Topic-slice
func (eventConsumer *EventConsumer) SubscribeTopics(topics messaging.Topics) error {
	if len(topics) == 0 {
		return errors.New("no topics provided")
	}

	eventConsumer.topics = &topics

	return nil
}

// SubscribeTopicPattern subscribes the EventConsumer to all topics matching the provided regexp pattern
func (eventConsumer *EventConsumer) SubscribeTopicPattern(regexp *regexp.Regexp) error {
	if regexp == nil {
		return errors.New("no topicpattern provided")
	}
	eventConsumer.topicPattern = regexp
	return nil
}

// Poll polls the topics subscribed to by the EventConsumer
func (eventConsumer *EventConsumer) Poll(ctx context.Context) (messaging.Message, messaging.Topic, error) {
	if eventConsumer.topics != nil && len(*eventConsumer.topics) != 0 {
		for _, topic := range *eventConsumer.topics {
			kafkaMessage, err := eventConsumer.store.Retrieve(ctx, topic.String())
			if err != nil {
				return messaging.Message{}, messaging.Topic{}, err
			}
			if kafkaMessage != nil {
				var message messaging.Message
				if eventConsumer.PreParseHook != nil {
					continueProcessing := eventConsumer.PreParseHook(topic, message)
					if !continueProcessing {
						return messaging.Message{}, messaging.Topic{}, errors.New("PreParseHook returned false, not continuing processing")
					}
				}
				message, err := ParseMessage(ctx, kafkaMessage)
				if err != nil {
					log.WithContext(ctx).Debug(err.Error())
				}
				return message, topic, nil
			}
		}
	} else if eventConsumer.topicPattern != nil {
		kafkaMessage, err := eventConsumer.store.RetrieveFromPattern(ctx, eventConsumer.topicPattern)
		if err != nil {
			return messaging.Message{}, messaging.Topic{}, err
		}
		if kafkaMessage != nil {
			var topic messaging.Topic
			if kafkaMessage.TopicPartition != (kafka.TopicPartition{}) && kafkaMessage.TopicPartition.Topic != nil {
				topic, err = messaging.TopicFromString(*kafkaMessage.TopicPartition.Topic)
				if err != nil {
					return messaging.Message{}, messaging.Topic{}, err
				}
			}
			var message messaging.Message
			err = json.Unmarshal(kafkaMessage.Value, &message)
			if err != nil {
				log.WithContext(ctx).Debug(err.Error())
			}
			return message, topic, nil
		}
	}
	return messaging.Message{}, messaging.Topic{}, nil
}

// ProcessMessage is...
func (eventConsumer *EventConsumer) ProcessMessage(ctx context.Context,
	consumedTopic messaging.Topic,
	receivedMessage messaging.Message,
	messageProcessedTopic *messaging.Topic,
	outgoingMessage *messaging.Message) error {
	if eventConsumer.PreProcessHook != nil {
		continueProcessing := eventConsumer.PreProcessHook(consumedTopic, receivedMessage)
		if !continueProcessing {
			return errors.New("PreProcessHook returned false, not continuing processing")
		}
	}

	eventConsumer.processor.ProcessMessage(ctx, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage)

	if eventConsumer.PostProcessHook != nil {
		continueProcessing := eventConsumer.PostProcessHook(consumedTopic, receivedMessage)
		if !continueProcessing {
			return errors.New("PostProcessHook returned false, not continuing processing")
		}
	}

	return nil
}

// Commit commits offset for currently assigned partitions
// This is a blocking call
func (eventConsumer *EventConsumer) Commit() error {
	// commit
	return nil
}

// SetPreParseHook is used to set a method that should be called before an incoming message is parsed
func (eventConsumer *EventConsumer) SetPreParseHook(preParseHook func(topic messaging.Topic, message messaging.Message) bool) {
	eventConsumer.PreParseHook = preParseHook
}

// SetPreProcessHook is used to set a method that should be called before an incoming message is processed
func (eventConsumer *EventConsumer) SetPreProcessHook(preProcessHook func(topic messaging.Topic, message messaging.Message) bool) {
	eventConsumer.PreProcessHook = preProcessHook
}

// SetPostProcessHook is used to set a method that should be called after an incoming message is processed
func (eventConsumer *EventConsumer) SetPostProcessHook(postProcessHook func(topic messaging.Topic, message messaging.Message) bool) {
	eventConsumer.PostProcessHook = postProcessHook
}

// Close does nothing and is only implemented to implement the interface
func (eventConsumer *EventConsumer) Close() {
	// do nothing
}
