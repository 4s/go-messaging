package messaging

// EventProducer is an interface for eventproducers
type EventProducer interface {
	Produce(topic Topic, message Message) error
	Close()
}
