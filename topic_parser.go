package messaging

import (
	"errors"
	"strings"
)

// TopicFromString creates a topic from the provided string
func TopicFromString(topicString string) (Topic, error) {
	if topicString == "" {
		return Topic{}, errors.New("topicString is empty")
	}

	t := Topic{}
	stringArray := strings.Split(topicString, TopicSeparator)

	pos := 0
	for _, subString := range stringArray {
		if pos == 0 {
			operation, error := OperationFromString(subString)
			if error != nil {
				return t, error
			}
			t.SetOperation(operation)
		}
		if pos == 1 {
			BodyCategory, error := BodyCategoryFromString(subString)
			if error != nil {
				return t, error
			}
			t.SetDataCategory(BodyCategory)
		}
		if pos == 2 {
			t.SetDataType(subString)
		}
		if pos >= 3 {
			t.AddDataCode(subString)
		}
		pos++
	}
	return t, nil
}
