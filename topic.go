package messaging

import (
	"errors"
)

// TopicSeparator is the string used to separate parts of a topic
var TopicSeparator = "_"

// Topic is a struct representing a Topic
type Topic struct {
	operation    Operation
	dataCategory BodyCategory
	dataType     string
	dataCodes    []string
}

// NewTopic functions as a constructor for Topic that returns a new instance of Topic
func NewTopic() Topic {
	return Topic{}
}

// NewTopicFromTopic functions as a constructor for Topic that returns a new instance of Topic based on the provided topic
func NewTopicFromTopic(topic Topic) Topic {
	t := Topic{}

	t.SetOperation(topic.GetOperation())
	t.SetDataCategory(topic.GetDataCategory())
	t.SetDataType(topic.GetDataType())
	t.SetDataCodes(topic.GetDataCodes())

	return t
}

// Operation is an enum for the set of valid topic Operations.
type Operation int

// The valid Operation values
const (
	// Events
	InputReceived Operation = iota + 1
	DataValidated
	DataConverted
	DataCreated
	DataUpdated
	TransactionCompleted
	ProcessingFailed
	Error
	// Commands
	Create
	Update
	Delete
)

func (o Operation) String() string {
	names := [...]string{
		"",
		"InputReceived",
		"DataValidated",
		"DataConverted",
		"DataCreated",
		"DataUpdated",
		"TransactionCompleted",
		"ProcessingFailed",
		"Error",
		"Create",
		"Update",
		"Delete"}

	if o > Delete || o < InputReceived {
		return "Unknown"
	}

	return names[o]
}

// SetOperation sets the Operation of the Topic
func (t *Topic) SetOperation(operation Operation) *Topic {
	t.operation = operation
	return t
}

// GetOperation returns the Operation of the topic
func (t *Topic) GetOperation() Operation {
	return t.operation
}

// OperationFromString parses a string into an Operation
func OperationFromString(operationString string) (Operation, error) {
	var operation Operation
	switch true {
	case operationString == InputReceived.String():
		operation = InputReceived
		return operation, nil
	case operationString == DataValidated.String():
		operation = DataValidated
		return operation, nil
	case operationString == DataConverted.String():
		operation = DataConverted
		return operation, nil
	case operationString == DataCreated.String():
		operation = DataCreated
		return operation, nil
	case operationString == DataUpdated.String():
		operation = DataUpdated
		return operation, nil
	case operationString == TransactionCompleted.String():
		operation = TransactionCompleted
		return operation, nil
	case operationString == ProcessingFailed.String():
		operation = ProcessingFailed
		return operation, nil
	case operationString == Error.String():
		operation = Error
		return operation, nil
	case operationString == Create.String():
		operation = Create
		return operation, nil
	case operationString == Update.String():
		operation = Update
		return operation, nil
	case operationString == Delete.String():
		operation = Delete
		return operation, nil
	default:
		return operation, errors.New("Unknown Operation")
	}
}

// SetDataCategory sets DataCategory of Topic
func (t *Topic) SetDataCategory(dataCategory BodyCategory) *Topic {
	t.dataCategory = dataCategory
	return t
}

// GetDataCategory returns the DataCategory of the topic
func (t *Topic) GetDataCategory() BodyCategory {
	return t.dataCategory
}

// SetDataType sets DataType of Topic
func (t *Topic) SetDataType(dataType string) *Topic {
	t.dataType = dataType
	return t
}

// GetDataType returns the DataType of the topic
func (t *Topic) GetDataType() string {
	return t.dataType
}

// SetDataCodes sets DataCodes of Topic
func (t *Topic) SetDataCodes(dataCodes []string) *Topic {
	t.dataCodes = dataCodes
	return t
}

// GetDataCodes returns the DataCodes of the topic
func (t *Topic) GetDataCodes() []string {
	return t.dataCodes
}

// AddDataCode adds a dataCode to the topics dataCode array
func (t *Topic) AddDataCode(dataCode string) *Topic {
	t.dataCodes = append(t.dataCodes, dataCode)
	return t
}

// ToString returns the topic as a human readable, dash-separated string
func (t *Topic) String() string {
	var topic string

	var operation Operation
	var dataCategory BodyCategory
	if t.operation != operation {
		topic += t.operation.String()
		if t.dataCategory != dataCategory {
			topic += TopicSeparator + t.dataCategory.String()
			if t.dataType != "" {
				topic += TopicSeparator + t.dataType
				for _, dataCode := range t.dataCodes {
					if dataCode != "" {
						topic += TopicSeparator + dataCode
					}
				}
			}
		}
	}

	return topic
}

// Equals checks whether the topic upon which this method was called, is equal to the provided other topic
func (t *Topic) Equals(other Topic) bool {
	if t.GetOperation() != other.GetOperation() {
		return false
	}
	if t.GetDataCategory() != other.GetDataCategory() {
		return false
	}
	if t.GetDataType() != other.GetDataType() {
		return false
	}
	if len(t.GetDataCodes()) != len(other.GetDataCodes()) {
		return false
	}
	for _, dataCode := range t.GetDataCodes() {
		exists := false
		for _, otherDataCode := range other.GetDataCodes() {
			if dataCode == otherDataCode {
				exists = true
			}
		}
		if !exists {
			return false
		}
	}
	return true
}

// Topics is a slice topics
type Topics []Topic

// ToString returns the topics as a human readable, comma-separated string
func (t Topics) String() string {
	var topicString string
	for _, value := range t {
		topicString += value.String() + ", "
	}
	return topicString
}

// StringArray returns the topics as an array og strings
func (t Topics) StringArray() []string {
	var stringTopics []string
	for _, value := range t {
		stringTopics = append(stringTopics, value.String())
	}
	return stringTopics
}
