package messaging

import (
	"bytes"
	"encoding/json"
	"strconv"
	"strings"
)

// Version is an object that represents a version in semantic versioning
type Version struct {
	Major int
	Minor int
	Patch int
}

var versionSeparator = "."

// NewVersion acts as a constructor for Version and returns a new instance of version, based on the provided versionstring
func NewVersion(versionString string) Version {
	if versionString == "" {
		return Version{}
	}
	versionSlice := strings.Split(versionString, ".")
	if len(versionSlice) < 2 {
		return Version{}
	}
	var err error
	var version Version
	version.Major, err = strconv.Atoi(versionSlice[0])
	version.Minor, err = strconv.Atoi(versionSlice[1])
	version.Patch, err = strconv.Atoi(versionSlice[2])
	if err != nil {
		return Version{}
	}
	return version
}

// IsCompatible compares two versions for compatibility
func (v Version) IsCompatible(version Version) bool {
	if v.Major == version.Major {
		return true
	}
	return false
}

func (v Version) String() string {
	return strconv.Itoa(v.Major) + versionSeparator + strconv.Itoa(v.Minor) + versionSeparator + strconv.Itoa(v.Patch)
}

// MarshalJSON marshals BodyCategory as a quoted json string
func (v Version) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(v.String())
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

// UnmarshalJSON unmashals a quoted json string to BodyCategory
func (v *Version) UnmarshalJSON(byteArray []byte) error {
	var j string
	err := json.Unmarshal(byteArray, &j)
	if err != nil {
		return err
	}
	// Note that if the string cannot be found then it will be set to the zero value, 'Created' in this case.
	*v = NewVersion(j)
	if err != nil {
		return err
	}
	return nil
}
