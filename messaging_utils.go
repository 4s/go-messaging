package messaging

import (
	"context"
	"math"
	"math/rand"
	"os"

	log "bitbucket.org/4s/go-logging"
)

var maxIDSize = 50

// GetEnv returns either an environmentvariable or the defaultvalue provided
func GetEnv(ctx context.Context, env string, defaultValue string) string {
	value := os.Getenv(env)
	if value != "" {
		return value
	}
	if defaultValue != "" {
		log.WithContext(ctx).Warnf("%v not set, using defaultvalue of %v", env, defaultValue)
	} else {
		log.WithContext(ctx).Warnf("%v not set", env)
	}
	return defaultValue
}

// VerifyOrCreateID checks if a correlationID exists and fits with the scheme of correlationId's used in this library.
// if it does, it returns the correlationID, if it doesn't it either adjusts it or generates a new one, and returns that
// ported from the method used in
// https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/MessagingUtils.java
func VerifyOrCreateID(correlationID string) string {
	if correlationID == "" {
		correlationID = generateCorrelationID()
	} else if len(correlationID) > maxIDSize {
		correlationID = correlationID[:maxIDSize]
	}

	return correlationID
}

func generateCorrelationID() string {
	random := rand.Int63()
	return encodeBase62(random)
}

// Copied from https://github.com/catinello/base62
// Copyright (c) 2016, Antonino Catinello
// All rights reserved.
func encodeBase62(n int64) string {
	base := int64(62)
	characterSet := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	b := make([]byte, 0)
	// loop as long the num is bigger than zero
	for n > 0 {
		// receive the rest
		r := math.Mod(float64(n), float64(base))
		// devide by Base
		n /= base
		// append chars
		b = append([]byte{characterSet[int(r)]}, b...)
	}
	return string(b)
}
