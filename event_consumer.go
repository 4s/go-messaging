package messaging

import (
	"context"
	"regexp"
)

// EventConsumer is an interface for event consumers
type EventConsumer interface {
	SubscribeTopics(topics Topics) error
	SubscribeTopicPattern(regexp *regexp.Regexp) error
	Poll(ctx context.Context) (Message, Topic, error)
	ProcessMessage(ctx context.Context,
		consumedTopic Topic,
		receivedMessage Message,
		messageProcessedTopic *Topic,
		outgoingMessage *Message) error
	Commit() error
	SetPreParseHook(func(topic Topic, message Message) bool)
	SetPreProcessHook(func(topic Topic, message Message) bool)
	SetPostProcessHook(func(topic Topic, message Message) bool)
	Close()
}
