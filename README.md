# Go Messaging library

A library that supports sending and receiving messages over the Apache Kafka messaging system. 

## Project wiki
See: [Main.md](docs//Main.md)

## Project status
4S maturity level: prototyping.

## Prerequisites
You need to have the following software installed

Go (golang) (https://golang.org/)
librdkafka (https://github.com/confluentinc/confluent-kafka-go#installing-librdkafka)

## Building:

This library does not contain a main package, and therefore cannot be built on its own.

## Usage

To use this library, it must be included in a project containing a main-package.

To include this library run the following command in your terminal/shell:

```bash
go get "bitbucket.org/4s/go-messaging"
```

After retrieving the library, the library can be included by importing it, in the files in which it is required:

```golang
package ...

import (
  ...
  messaging "bitbucket.org/4s/go-messaging"
)

...

message := messaging.NewMessage()
...
```

## Environment variables
The file `kafka.env` contain the environment variables that you need to use if you include and use this 
library in your own services.

The environmentvariables `CORRELATION_ID` and `TRANSACTION_ID` denotes the JSON keys for the CorrelationID and TransactionID in log output.

**kafka.env environment variables:**
`KAFKA_BOOTSTRAP_SERVER` should be set to the host and port of your Kafka server. I.e.  the host/port pair to use for 
establishing the initial connection to the Kafka cluster. This corresponds to the native Kafka producer and consumer 
config called *bootstrap.servers* - see https://kafka.apache.org/documentation/#configuration - except that the messaging 
library currently only supports one server, not a list of servers like native Kafka does.

All other environment variables starting with ``KAFKA_`` map directly to a corresponding [Kafka configuration key](https://kafka.apache.org/documentation/#configuration) 
following a scheme where you strip off  the ``KAFKA_`` part and replace `_` with a `.`. For instance `KAFKA_KEY_DESERIALIZER`
corresponds to the native Kafka consumer property `key.deserializer`

You should not change the values of `KAFKA_ENABLE_AUTO_COMMIT`.`

Currently only the following Kafka related environment variables are supported:
```
KAFKA_BOOTSTRAP_SERVER

#KafkaConsumer
KAFKA_ENABLE_AUTO_COMMIT
KAFKA_AUTO_COMMIT_INTERVAL_MS
KAFKA_SESSION_TIMEOUT_MS
KAFKA_GROUP_ID

#KafkaProducer
KAFKA_ACKS
KAFKA_RETRIES
```
### Consumer health
With the following environment variables you may enable that instances of the KafkaProcessAndConsume class touch a 
file at specified intervals:
``` 
ENABLE_HEALTH=true
HEALTH_FILE_PATH=/tmp/health
HEALTH_INTERVAL_MS=5000
```
This can be used to check the health of the KafkaProcessAndConsume thread - typically be having a shell script check 
on the timestamp of the health file.

## Test
__To run unit tests:__
```
go test ./test
```

__To run integration tests:__

For the integration tests to succeed you need access to a running instance of Apache Kafka and 
KAFKA_BOOTSTRAP_SERVER in kafka.env must point to the Kafka broker of this instance. 

Furthermore you need to ensure the instance of Kafka you're testing against doesn't already have data on the topics
you are testing against. To ensure this delete these topics (assumes your kafka is running inside a docker container):

```
docker exec <docker-container-name> /opt/kafka/bin/kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic InputReceived_FHIR_Observation_MDC29112
docker exec <docker-container-name> /opt/kafka/bin/kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic DataCreated_FHIR_Observation_MDC29112
```

Now run:
```
go test -tags integration
```