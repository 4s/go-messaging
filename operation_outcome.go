package messaging

// OperationOutcome is a collection of error, warning or information messages that result from a system action.
type OperationOutcome struct {
	ResourceType string                `json:"resourceType,omitempty"`
	ID           string                `json:"id,omitempty"`
	Text         interface{}           `json:"text,omitempty"`
	Issue        OperationOutcomeIssue `json:"issue,omitempty"`
}

// OperationOutcomeIssue is a single issue associated with the action
type OperationOutcomeIssue struct {
	Severity    string          `json:"severity,omitempty"`
	Code        string          `json:"code,omitempty"`
	Details     CodeableConcept `json:"details,omitempty"`
	Diagnostics string          `json:"diagnostics,omitempty"`
	Location    string          `json:"location,omitempty"`
	Expression  string          `json:"expression,omitempty"`
}

// CodeableConcept represents a value that is usually supplied by providing a reference to one or more terminologies or
// ontologies but may also be defined by the provision of text.
type CodeableConcept struct {
	Coding []Coding `json:"coding,omitempty"`
	Text   string   `json:"text,omitempty"`
}

// Coding is a representation of a defined concept using a symbol from a defined "code system"
type Coding struct {
	System       string `json:"system,omitempty"`
	Version      string `json:"version,omitempty"`
	Code         string `json:"code,omitempty"`
	Display      string `json:"display,omitempty"`
	UserSelected bool   `json:"userSelected,omitempty"`
}

// NewOperationOutcome functions as a constructor for OperationOutcome and returns a new instance of OperationOutcome
func NewOperationOutcome(severity string, code string, diagnostics string) OperationOutcome {
	operationOutcome := OperationOutcome{
		ResourceType: "OperationOutcome",
	}
	operationOutcome.SetSeverity(severity).
		SetCode(code).
		SetDiagnostics(diagnostics)

	return operationOutcome
}

// GetSeverity returns the Severity of the OperationOutcome
func (ou *OperationOutcome) GetSeverity() string {
	return ou.Issue.Severity
}

// SetSeverity sets the Severity of the OperationOutcome
func (ou *OperationOutcome) SetSeverity(severity string) *OperationOutcome {
	ou.Issue.Severity = severity
	return ou
}

// GetCode returns the Code of the OperationOutcome
func (ou *OperationOutcome) GetCode() string {
	return ou.Issue.Code
}

// SetCode sets the Code of the OperationOutcome
func (ou *OperationOutcome) SetCode(code string) *OperationOutcome {
	ou.Issue.Code = code
	return ou
}

// GetDiagnostics returns the Diagnostics of the OperationOutcome
func (ou *OperationOutcome) GetDiagnostics() string {
	return ou.Issue.Diagnostics
}

// SetDiagnostics sets the Diagnostics of the OperationOutcome
func (ou *OperationOutcome) SetDiagnostics(diagnostics string) *OperationOutcome {
	ou.Issue.Diagnostics = diagnostics
	return ou
}
