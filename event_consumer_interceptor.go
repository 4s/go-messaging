package messaging

// EventConsumerInterceptor is a ... TODO:
type EventConsumerInterceptor interface {
	// IncomingMessagePreParsing is called before any other processing takes place for each incoming message. It may be e.g. be used
	// to provide alternate processing of incoming messages.
	IncomingMessagePreParsing(consumedTopic Topic, receivedMessage Message) bool
	// IncomingMessagePreProcessMessage is called for each message, immediately after parsing of incoming topic and message strings and
	// before any other processing takes place for each incoming message. It may e.g. be used to provide alternate processing of incoming messages.
	IncomingMessagePreProcessMessage(consumedTopic Topic, receivedMessage Message) bool
	// IncomingMessagePostProcessMessage is called for each message, immediately after processing the incoming message, but before any
	// response is sent out be the {@link EventConsumer}. It may e.g. be used to provide alternate responses to incoming messages.
	IncomingMessagePostProcessMessage(consumedTopic Topic, receivedMessage Message) bool
	// HandleError is called upon any exception being thrown within the {@link EventConsumer's} processing code.
	// This includes parse exceptions and message incompatibility exceptions.
	HandleError(consumedTopic Topic, receivedMessage Message, err error) bool
	// ProcessingCompletedNormally is called after all processing of an incoming message is completed, but only if the
	// request completes normally (i.e. no exception is thrown).
	ProcessingCompletedNormally(consumedTopic Topic, receivedMessage Message)
}
