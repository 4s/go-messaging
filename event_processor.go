package messaging

import "context"

// EventProcessor is an interface for EventProcessors
type EventProcessor interface {
	ProcessMessage(ctx context.Context,
		consumedTopic Topic,
		receivedMessage Message,
		messageProcessedTopic *Topic,
		outgoingMessage *Message) error
}
