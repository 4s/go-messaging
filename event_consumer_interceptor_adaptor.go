package messaging

// EventConsumerInterceptorAdaptor is a struct implementing all methods for EventConsumerInterceptor, providing a No-op implementagion of all
// methods, always returning true
type EventConsumerInterceptorAdaptor struct{}

// IncomingMessagePreParsing is called before any other processing takes place for each incoming message. It may be e.g. be used
// to provide alternate processing of incoming messages.
func (eventConsumerInterceptorAdaptor EventConsumerInterceptorAdaptor) IncomingMessagePreParsing(consumedTopic Topic, receivedMessage Message) bool {
	return true
}

// IncomingMessagePreProcessMessage is called for each message, immediately after parsing of incoming topic and message strings and
// before any other processing takes place for each incoming message. It may e.g. be used to provide alternate processing of incoming messages.
func (eventConsumerInterceptorAdaptor EventConsumerInterceptorAdaptor) IncomingMessagePreProcessMessage(consumedTopic Topic, receivedMessage Message) bool {
	return true
}

// IncomingMessagePostProcessMessage is called for each message, immediately after processing the incoming message, but before any
// response is sent out be the {@link EventConsumer}. It may e.g. be used to provide alternate responses to incoming messages.
func (eventConsumerInterceptorAdaptor EventConsumerInterceptorAdaptor) IncomingMessagePostProcessMessage(consumedTopic Topic, receivedMessage Message) bool {
	return true
}

// HandleError is called upon any exception being thrown within the {@link EventConsumer's} processing code.
// This includes parse exceptions and message incompatibility exceptions.
func (eventConsumerInterceptorAdaptor EventConsumerInterceptorAdaptor) HandleError(consumedTopic Topic, receivedMessage Message, err error) bool {
	return true
}

// ProcessingCompletedNormally is called after all processing of an incoming message is completed, but only if the
// request completes normally (i.e. no exception is thrown).
func (eventConsumerInterceptorAdaptor EventConsumerInterceptorAdaptor) ProcessingCompletedNormally(consumedTopic Topic, receivedMessage Message) {
}
